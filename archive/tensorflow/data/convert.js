const fs = require('fs');

const raw = fs.readFileSync('./source.csv').toString();

const arr = [];
raw.split('\n').forEach(line => {
  const data = line.split(',');
  if (data[1] && data[3]) {
    const sentiment = data[1].trim();
    const text = data[3].trim();
    arr.push(text + '\t' + sentiment);
  }
});

fs.writeFileSync('./source.txt', arr.join('\n'))
console.log(arr);