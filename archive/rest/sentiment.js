const AWS = require('aws-sdk');
const request = require('request');
AWS.config.update({region: 'us-east-1'});

module.exports = (event, context, callback) => {

  const data = event.data ? event.data : '';
  const url = 'http://35.168.11.142:3000/?data=' + data;

  // headers
  const headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'
  };

  // log
  const log = (type, url, err) => {
    console.log(type + ' ' + url);
    console.log(err);
  };

  // get response
  const getResponse = (data) => {
    const response = {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Origin" : "*",
        "Access-Control-Allow-Credentials" : true
      },
      body: data,
      isBase64Encoded: false
    };
    return response;
  }

  request({ uri: url, method: 'GET', timeout: 2000, headers: headers }, (error, response, body) => {
    console.log(url);
    if (error) {
      log('error request', url, error);
      callback(null, getResponse(error));
    } else {
      callback(null, getResponse(body));
    }
  });


}
