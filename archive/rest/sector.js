const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});

module.exports = (event, context, callback) => {
  // constant
  const SECTORS_TABLE = process.env.SECTOR_TABLE;
  const IS_OFFLINE = process.env.IS_OFFLINE;

  // database
  const getDynamoRemote = () => new AWS.DynamoDB.DocumentClient();
  const getDynamoDbOffline = () => new AWS.DynamoDB.DocumentClient({region: 'localhost', endpoint: 'http://localhost:3000'});
  const dynamoDb = IS_OFFLINE ? getDynamoDbOffline() : getDynamoRemote();

  // get all items with sentiment empty
  const queryP = (callback) => {
    const params = {
      TableName: SECTORS_TABLE ? SECTORS_TABLE : 'Sectors-production',
    };

    return dynamoDb.scan(params, callback);
  };

  queryP((err, data) => {
    const promises = [];
    console.log(data);

    if (err) {
      callback(null, err);
    }

    callback(null, data);
  });

}

