const AWS = require('aws-sdk');
const utils = require('./../utils/util-rss');
const util = require('./../utils/util');
AWS.config.update({region: 'us-east-1'});

module.exports = (event, context, callback) => {
  // constant
  const SECTORS_TABLE = process.env.SECTOR_TABLE;
  const IS_OFFLINE = process.env.IS_OFFLINE;

  // database
  const getDynamoRemote = () => new AWS.DynamoDB.DocumentClient();
  const getDynamoDbOffline = () => new AWS.DynamoDB.DocumentClient({region: 'localhost', endpoint: 'http://localhost:3000'});
  const dynamoDb = IS_OFFLINE ? getDynamoDbOffline() : getDynamoRemote();

  const url = 'https://www.alphavantage.co/query?function=SECTOR&apikey=L4PBG6OHWXRK9FTF';
  utils.get(url, (junk, data) => {

    // insert data
    const itemData = {
      data: data,
      hash_key: 'today'
    };
    console.log(itemData);

    const params = {
      TableName: SECTORS_TABLE ? SECTORS_TABLE : 'Sectors-production',
      Item: util.stringifyObj(itemData),
      ReturnValues: 'ALL_OLD'
    };

    dynamoDb.put(params, (values, err) => {
      if (err) callback(null, err);
      callback(null, values);
    });

  }, (junk, err) => {
    callback(null, err);
  });
}

