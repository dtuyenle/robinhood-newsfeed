const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});

module.exports = (event, context, callback) => {

  const stock_id = event.stock ? event.stock : 'AAPL';

  // constant
  const RISK_TABLE = process.env.RISK_TABLE;
  const IS_OFFLINE = process.env.IS_OFFLINE;

  // database
  const getDynamoRemote = () => new AWS.DynamoDB.DocumentClient();
  const getDynamoDbOffline = () => new AWS.DynamoDB.DocumentClient({region: 'localhost', endpoint: 'http://localhost:3000'});
  const dynamoDb = IS_OFFLINE ? getDynamoDbOffline() : getDynamoRemote();

  // get item with stockId
  const queryP = (callback) => {
    const params = {
      TableName: RISK_TABLE ? RISK_TABLE : 'Risk-production',
      KeyConditionExpression: 'hash_key = :hash_key',
      ExpressionAttributeValues: {
        ":hash_key": stock_id
      }
    };

    return dynamoDb.query(params, callback);
  };

  // get response
  const getResponse = (data) => {
    const response = {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Origin" : "*",
        "Access-Control-Allow-Credentials" : true
      },
      body: data,
      isBase64Encoded: false
    };
    return response;
  }

  queryP((err, data) => {
    const promises = [];
    console.log(data);

    if (err) {
      callback(null, getResponse(err));
    }

    callback(null, getResponse(data));
  });

}

