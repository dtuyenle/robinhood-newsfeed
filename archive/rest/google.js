const AWS = require('aws-sdk');
const utils = require('./../utils/util-rss');
AWS.config.update({region: 'us-east-1'});

module.exports = (event, context, callback) => {

  const stock_id = event.stock ? event.stock : 'AAPL';
  const url = 'https://news.google.com/news/feeds?output=rss&q=' + stock_id;

  utils.rss(url, callback);
}
