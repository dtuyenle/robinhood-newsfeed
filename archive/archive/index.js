// rest
module.exports.google = require('./rest/google');
module.exports.bing = require('./rest/bing');
module.exports.nasdaq = require('./rest/nasdaq');
module.exports.seekingalpha = require('./rest/seekingalpha');
module.exports.sentiment = require('./rest/sentiment');
module.exports.sector = require('./rest/sector');
module.exports.risk = require('./rest/risk');

// job
module.exports.sectorperf = require('./rest/sectorperf');
module.exports.riskperf_nasdaq1 = require('./rest/riskperf_nasdaq1');
module.exports.riskperf_nyse1 = require('./rest/riskperf_nyse1');
module.exports.riskperf_nasdaq2 = require('./rest/riskperf_nasdaq2');
module.exports.riskperf_nyse2 = require('./rest/riskperf_nyse2');