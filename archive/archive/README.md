# robinhood-extension

Feed
https://news.google.com/news/feeds?output=rss&q=
https://www.bing.com/news/search?format=rss&q=
http://articlefeeds.nasdaq.com/nasdaq/symbols?symbol=AAPL
https://www.marketwatch.com/rss/
https://seekingalpha.com/api/sa/combined/AAPL.xml

Lambda function
+ feed
google feed and bing feed

1. Call rss google/bing endpoint and get data
2. Parse data
3. Check for duplicate - get latest pub date item form dynamodb and compare with the new data if the news item having date bigger than insert otherwise ignore
4. Store data into dynamodb


+ get-news
1. Request comes in - includes timestamp of latest news
2. Check if new data is in database - get all data larger than timestamp
3. https://stackoverflow.com/questions/31714788/can-an-aws-lambda-function-call-another
4. Return to client

+ sentiment analysis
1. Run every 10 mins
2. Mark as positive or negative

+ backup
backup dynamodb every day into s3


DynamoDB
Log
+ stock_id P
+ count
+ date

News
+ hash_key from guid or title
+ stock_id P
+ sentiment
+ source
+ title
+ author
+ description P
+ posted_date P
+ created_date



Sentiments
Using this AFINN lib until have a tensorflow model
+ https://github.com/thisandagain/sentiment



+ keras js
python3 ./encoder.py model.h5



tensorflowjs_converter --input_format keras /model.h5 /js


