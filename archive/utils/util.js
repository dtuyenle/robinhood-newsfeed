const stringify = str => String(str).trim();

const stringifyObj = obj => {
  Object.keys(obj).forEach(key => {
    obj[key] = obj[key] ? stringify(obj[key]) : 'n/a';
  });
  return obj;
};

module.exports = {
  stringify: stringify,
  stringifyObj: stringifyObj
};
