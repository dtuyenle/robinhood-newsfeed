const AWS = require('aws-sdk');
const Parser = require('rss-parser');
const parser = new Parser();
const request = require('request');
const utils = require('./util');
AWS.config.update({region: 'us-east-1'});

// headers
const headers = {
  'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36'
};

// log
const log = (type, url, err) => {
  console.log(type + ' ' + url);
  console.log(err);
};

// get response
const getResponse = (data) => {
  const response = {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Origin" : "*",
      "Access-Control-Allow-Credentials" : true
    },
    body: data,
    isBase64Encoded: false
  };
  return response;
}

// parse rss to json
const parseRSS = (rss, callback) => {
  try {
    if (rss.indexOf('<!DOCTYPE html>') !== -1) {
      log('html page');
      callback({items: []});
    } else {
      parser.parseString(rss).then(callback);
    }
  } catch(e) {
    log('error parsing');
    callback({items: []});
  }
};

const get = (url, callback, callbackErr) => {
  request({ uri: url, method: 'GET', timeout: 2000, headers: headers }, (error, response, body) => {
    if (error) {
      log('error request', url, error);
      callbackErr(null, error);
    } else {
      callback(null, body);
    }
  });
};

const rss = (url, callback) => {

  // parse rss to json
  const parseRSS = (rss, callback) => {
    try {
      if (rss.indexOf('<!DOCTYPE html>') !== -1) {
        log('html page');
        callback({items: []});
      } else {
        parser.parseString(rss).then(callback);
      }
    } catch(e) {
      log('error parsing');
      callback({items: []});
    }
  };

  // ignore
  const ignores = [
    'This RSS feed URL is deprecated'
  ];

  request({ uri: url, method: 'GET', timeout: 2000, headers: headers }, (error, response, body) => {
    console.log(url);
    if (error) {
      log('error request', url, error);
      callback(null, getResponse(error));
    } else {
      parseRSS(body, data => {
        callback(null, getResponse(data));
      });
    }
  });
};

module.exports = {
  rss: rss,
  get: get
};
