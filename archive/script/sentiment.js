const AWS = require('aws-sdk');
const util = require('./../utils/util');
AWS.config.update({region: 'us-east-1'});
const fs = require('fs');

// read data
const rawYelp = fs.readFileSync('./../tensorflow/data/yelp_labelled.txt').toString();
const rawAmazon = fs.readFileSync('./../tensorflow/data/amazon_cells_labelled.txt').toString();
const rawImdb = fs.readFileSync('./../tensorflow/data/imdb_labelled.txt').toString();
const rawSource = fs.readFileSync('./../tensorflow/data/source.txt').toString();

// stop at 903428

const raw = rawSource; // + rawYelp+ rawAmazon + rawImdb;

const parseData = [];

raw.split('\n').forEach(line => {
  const lineArr = line.split('\t');
  parseData.push({
    text: lineArr[0],
    sentiment: lineArr[1]
  });
});

// constant
const SENTIMENT_TABLE = 'Sentiment-production';

// database
const getDynamoRemote = () => new AWS.DynamoDB.DocumentClient();
const dynamoDb = getDynamoRemote();

let count = 903429;
const total = parseData.length;
function insertData(item) {
  // insert data
  const itemData = {
    sentiment: item.sentiment,
    data: item.text,
    hash_key: item.text.split(' ').join('').slice(0, 100)
  };
  console.log(itemData);

  const params = {
    TableName: SENTIMENT_TABLE,
    Item: util.stringifyObj(itemData),
    ReturnValues: 'ALL_OLD'
  };

  dynamoDb.put(params, (values, err) => {
    if (err && err !== null) console.log(err);
    if (count === total) {
      console.log(values);
    } else {
      count++;
      if (parseData[count]) {
        insertData(parseData[count]);
      }
    }
  });
}

insertData(parseData[count]);

