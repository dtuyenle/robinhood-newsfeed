Real time news with sentiment analysis for daily stock trader! Add on useful features such as: profit display, sector performance...
-------- THIS EXTENSION IS FREE ---------------

IMPORTANT! You don't have to click on the extension to make it work. It automatically works when you go to a specific stock page on robinhood website and it only works on robinhood website.

-------------------- Note: I been receiving feedback regarding the extensions not loading. I am very sorry, I made an update and It should fix now. If you have any other issue, please "refresh the browser" and just leave a review from chrome store and I will fix it right away. Please don't uninstall and give us a chance to improve. -------------------

Robinews The must have addon for robinhood website

Have you ever using robinhood website to invest your future saving ? Or just to make a quick bucks as a daily trader ? Have you ever wonder what if I could see all related news about a specific stock right on the page so that I can make decisions better ? If yes then this extensions is just right for you.

Robinews is a lightweight and useful add-in designed for getting a quick peek of market news and information for your favorite stocks such as Apple, Google, Yahoo, Facebook, Microsoft, Nasdaq, Down Jones etc.

* This extension is perfect for daily trader since you get to see all news that have potential impacting the pricing movement of stocks.
* This extension will help you to determine if you should buy a sepcific stock with real time data (news and information coming from multiple sources then run through sentiment analysis).
* This extention also helps you to see what profit or losses when you set "limit price" and "stop loss price"
* This extension will show you the sector performance of current stocks. This way you know if the whole sector having potential to invest.
* This extension will show you stock rating and letter grade (from nasdaq) so that you know subjectively if you should buy the stock

Get real time stock market information about your favorite stocks.
This extension adds a "Live Feed" section right under "Collections" and it shows you the latest stock news in the dynamic moving market.
You get real time information from multiple sources. Each news are then run through our sentiment analysis model to determine if a news is "positive" or "negative".

You see near real time stock information with sentiment just below Robinhood "Collections" section.
If you want to know more details about that news, you can click on the title.
It then opens a new page and take you to the source.

A few great features in this browser extension:
+ Real time stock information with sentiment
+ Auto calculate your profit "winning" or "losing" when you enter "limit price" or "stop loss price"
+ Show the movement performance of the stock sector.
+ Current rating for your stock



PRIVACY CONCERN ? BELOW IS OUR PRIVACY POLICY

Robinews Privacy Policy
Effective date: May 22, 2018

Thank you for using Robinews! We take your privacy extremely seriously and would like to describe how we collect, use and protect your information when you access our website(s), products, services and applications (collectively, the “Services”).

We only use Google Analytic to collect information regarding how you use the app. Other than that we collect and use zero personal information from you.


Will Robinews ever change this Privacy Policy?
No

What Information does Robinews Collect?
We collect zero information from you. We only use google anlaytic to understand how you use the extension to enhance or implement new features.
