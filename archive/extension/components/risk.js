(( Robinews, jQuery ) => {

  async function loadRiskPerf() {
    if (Robinews.isStockPage()) {

      jQuery('#risk').remove();

      const stockId = jQuery('.card-heading h3').first().text().split(' ')[1] ? jQuery('.card-heading h3').first().text().split(' ')[1].trim() : '';
      const data = await fetch('https://7dc142xl39.execute-api.us-east-1.amazonaws.com/production/risk?stock=' + stockId);
      const json = await data.json();
      const risk = JSON.parse(json.body && typeof json.body.Items[0] !== 'undefined' ? json.body.Items[0].data : {});

      const rating = risk.response.docs[0].CurrentRating;
      const grade = risk.response.docs[0].LetterGradeRating;

      const color = rating === 'Buy' ? '#21ce99' : '#f45531';
      jQuery('.sidebar-content').find('form').first().parent().append('<div style="width: 100%; border: 1px solid #0e0d0d; border-radius: 2px; padding: 15px;" id="risk"></div>');
      jQuery('#risk').append('<div style="line-height: 26px; color: ' + color + '">Current Rating: ' + rating + '</div>');
      jQuery('#risk').append('<div style="line-height: 26px; color: ' + color + '">Letter Grade Rating: ' + grade + '</div>');
    }
  };

  Robinews.Publisher.listeners.push(loadRiskPerf);

})( window.Robinews = window.Robinews || {}, jQuery );
