(( Robinews, jQuery ) => {

  async function loadSectorPerf() {
    if (Robinews.isStockPage()) {
      const stocks = Robinews.stocks;
      const mapping = {
        'Basic Industries': 'Industrials',
        'Capital Goods': 'Materials',
        'Consumer Durables': 'Consumer Staples',
        'Consumer Non-Durables': 'Consumer Discretionary',
        'Consumer Services': 'Consumer Discretionary',
        'Energy': 'Energy',
        'Finance': 'Financials',
        'Health Care': 'Health Care',
        'Miscellaneous': '',
        'Public Utilities': 'Utilities',
        'Technology': 'Information Technology',
        'Transportation': ''
      }

      const getSector = stockId => {
        const amex = stocks.amex.filter(stock => stock.Symbol === stockId);
        if(amex.length > 0) return mapping[amex[0].Sector] ? mapping[amex[0].Sector] : amex[0].Sector;
        const nasdaq = stocks.nasdaq.filter(stock => stock.Symbol === stockId);
        if(nasdaq.length > 0) return mapping[nasdaq[0].Sector] ? mapping[nasdaq[0].Sector] : nasdaq[0].Sector;
        const nyse = stocks.nyse.filter(stock => stock.Symbol === stockId);
        if(nyse.length > 0) return mapping[nyse[0].Sector] ? mapping[nyse[0].Sector] : nyse[0].Sector;
      }
      const compareChars = (str1, str2) => {
        let score = 0;
        str1.split('').forEach((char1, index1) => {
          if (char1 === str2.split('')[index1]) {
            score++;
          }
        });
        return str1.length - score;
      };

      jQuery('#sector-indicator').remove();
      const stockId = jQuery('.card-heading h3').first().text().split(' ')[1] ? jQuery('.card-heading h3').first().text().split(' ')[1].trim() : '';
      const data = await fetch('https://7dc142xl39.execute-api.us-east-1.amazonaws.com/production/sector');
      const json = await data.json();
      const sectors = JSON.parse(json.Items[0].data)['Rank A: Real-Time Performance'];

      let already = false;
      jQuery('.main-container').find('div').eq(0).find('a').each((index, item) => {
        const elem = jQuery(item).find('span').eq(0);
        const sector = elem.text();

        Object.keys(sectors).forEach(key => {
          if (!already && (key.includes(sector) || sector.includes(key) || compareChars(sector, key) < 4)) {
            const style = `background: ${sectors[key].includes('-') ? '#f45531' : '#21ce99'}; color: white; margin-left: 5px; padding: 0px 10px; border-radius: 15px;`;
            elem.html(sectors[key] ? sector + '<span id="sector-indicator" style="' + style + '">' +  sectors[key] + '</span>' : sector)
            already = true;
          }
        });

        if (!already) {
          const key = getSector(stockId);
          if (key && key !== '') {
            const style =  `background: ${sectors[key] && sectors[key].includes('-') ? '#f45531' : '#21ce99'}; color: white; margin-left: 5px; padding: 2px 10px; border-radius: 15px;`;
            jQuery(item).parent().append(sectors[key] ? '<a id="sector-indicator" style="-webkit-box-align: center; -ms-flex-align: center; align-items: center; border-radius: 17px; display: -webkit-inline-box; display: -ms-inline-flexbox; display: inline-flex; height: 28px; overflow: hidden; padding: 0 12px 1px;"><div></div><span style="' + style + '">' + key + ': ' +  sectors[key] + '</span></a>' : '');
            already = true;
          }
        }

        if (!already) {
          jQuery(item).parent().append('<span style="display: none" id="sector-indicator"></a>');
          already = true;
        }

      });
    }
  };

  Robinews.Publisher.listeners.push(loadSectorPerf);

})( window.Robinews = window.Robinews || {}, jQuery );
