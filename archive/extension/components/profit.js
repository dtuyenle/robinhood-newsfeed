(( Robinews, jQuery ) => {

  const addProfitIndicator = () => {

    if (Robinews.isStockPage()) {

      const getNumber = str => {
        const numb = str.replace('$', '').replace(/[,]/g, '').trim();
        return numb === '' ? 0 : parseFloat(numb);
      };


      if (jQuery('#profit-indicator').length === 0) {
        jQuery('.card-heading').parent().prepend('<div id="profit-indicator"></div>');

        const handleKeyUp = (number) => {
          const currTab = jQuery('.active').find('h3').text().indexOf('Buy') !== -1 ? 'buy' : 'sell';

          if (currTab === 'sell') {
            jQuery('#profit-indicator').show();
          } else {
            jQuery('#profit-indicator').hide();
          }

          // price
          const stopPrice = jQuery('input[name=stop_price]').val() ? getNumber(jQuery('input[name=stop_price]').val()) : 0;
          const limitPrice = jQuery('input[name=price]').val() ? getNumber(jQuery('input[name=price]').val()) : 0;
          const inputPrice = jQuery('input[name=stop_price]').val() ? stopPrice : limitPrice;

          const marketPrice = getNumber(jQuery('.main-container').find('header').eq(1).find('h1').first().text().split('$')[1]);
          const price = typeof jQuery('input[name=price]').css('display') === 'undefined' && typeof jQuery('input[name=stop_price]').css('display') === 'undefined' ? marketPrice : inputPrice;
          const cost = getNumber(jQuery('.grid-2').find('header').eq(1).find('h2').text());

          // share
          const currShare = getNumber(jQuery('.table').eq(1).find('td').eq(2).text());
          const share = getNumber(jQuery('input[name=quantity]').val());
          const total = typeof jQuery('input[name=quantity]').css('display') === 'undefined' ? currShare : share;

          // profit
          const profit = Math.round((price - cost) * total);

          const text = profit < 0 ? 'Losing: ' + profit : 'Winning: $' + profit;
          jQuery('#profit-indicator').text(text);
          jQuery('#profit-indicator').css({color: profit > 0 ? '#21ce99' : '#f45531', marginTop: '10px'});
        }

        jQuery(document).on('change keyup paste', 'input[name=quantity]', function() {
          handleKeyUp();
        });
        jQuery(document).on('change keyup paste', 'input[name=price]', function() {
          handleKeyUp();
        });
      }
    }
  };

  Robinews.Publisher.listeners.push(addProfitIndicator);
})( window.Robinews = window.Robinews || {}, jQuery );
