(( Robinews, jQuery ) => {
  const start = () => {
    if (Robinews.isStockPage()) {

      const stockId = jQuery('.card-heading h3').first().text().split(' ')[1] ? jQuery('.card-heading h3').first().text().split(' ')[1].trim() : '';
      const stockName = jQuery('h1').first().text();

      const cleanup = (str) => {
        let string = str.trim().toLowerCase();
        string = string.replace(/(\.|\,|\!)/g, '');
        string = string.replace('undefined', '');
        string = string.replace(/\/n/g, '');
        string = string.split(' ').filter(char => char !== '');
        return string;
      }

      async function loadSentiment() {
        const model = await tf.loadModel('https://s3.amazonaws.com/sentiment-nlp/tensorflow/model.json');
        const metadata = (await fetch("https://s3.amazonaws.com/sentiment-nlp/tensorflow/dictionary.json")).json();
        const labels = ['negative', 'positive'];
        metadata.then(data => {
          jQuery('.sentiment').each((index, item) => {
            const text = jQuery(item).parent().parent().text();
            const trimmed = cleanup(text);

            const inputBuffer = tf.buffer([1, 3000], "float32");
            trimmed.forEach((word, i) => inputBuffer.set(data[word], 0, data[word]));
            const input = inputBuffer.toTensor();
            const prediction = model.predict(input);
            const positivity = prediction.dataSync();
            prediction.dispose();

            const indx = positivity[0] > positivity[1] ? 0 : 1;
            const sentiment = labels[indx];
            jQuery(item).text(sentiment);
            jQuery(item).attr('style', formatSentiment(sentiment));
          });
        });
      }

      const getPromise = (sourceName, stockName) => {
        const url = 'https://7dc142xl39.execute-api.us-east-1.amazonaws.com/production/';
        return fetch(url + sourceName + '?stock=' + stockName).then(res => res.json());
      };

      async function parallel(stockId, stockName) {
        const google = getPromise('google', encodeURIComponent(stockName.replace(/[^a-zA-Z0-9]+/g, '')) + ' stock market');
        const bing = getPromise('bing', encodeURIComponent(stockName.replace(/[^a-zA-Z0-9]+/g, '')) + ' stock market');
        const nasdaq = getPromise('nasdaq', encodeURIComponent(stockId));
        const seekingalpha = getPromise('seekingalpha', encodeURIComponent(stockId));

        return {
          google: await google,
          bing: await bing,
          nasdaq: await nasdaq,
          seekingalpha: await seekingalpha
        };
      }

      async function taskRunner(fn, stockId, stockName, callback) {
        const result = await fn(stockId, stockName);
        let data = result.google === null ? [] : result.google.body.items.filter(item => { item.source = 'google'; return item.title !== 'This RSS feed URL is deprecated'; });
        data = result.bing === null ? [] : data.concat(result.bing.body.items.map(item => { item.source = 'bing'; return item; }));
        data = result.nasdaq === null ? [] : data.concat(result.nasdaq.body.items.map(item => { item.source = 'nasdaq'; return item; }));
        data = result.seekingalpha === null ? [] : data.concat(result.seekingalpha.body.items.map(item => { item.source = 'seekingalpha'; return item; }));
        callback(data);
      }

      const style = {
        container: 'height: 500px; width: 100%; overflow: hidden; margin-bottom: 60px',
        containerScroll: 'width: 100%; height: 100%; overflow-y: scroll; padding-right: 17px; box-sizing: content-box',
        item: 'padding: 10px 0; margin: 16px 0',
        header: 'display: flex; justify-content: space-between; margin-bottom: 5px',
        title: (Robinews.isOpen() ? 'color: #21ce99;' : 'color: #21ce99;') + 'margin: 0 0 5px; width: 500px; font-size: 15px;',
        date: (Robinews.isOpen() ? 'color: #666;' : 'color: #FFF;') + 'margin: 0 0 5px;',
        source: 'color: #8c8c8e',
        sentiment: 'margin: 0 0 5px',
        description: 'font-size: 14px',
        label: (Robinews.isOpen() ? 'color: #000;' : 'color: #FFF;') + 'font-weight: 900; border-bottom: 1px solid #0e0d0d; font-size: 25px; padding: 20px 2px'
      };

      const sortByDate = (array) => {
        array.sort((a, b) => {
          return new Date(b.pubDate) - new Date(a.pubDate);
        });
        return array;
      };

      const formatDate = date => {
        return moment(date).fromNow().replace(' ago', '').replace('days', 'd').replace('hours', 'h');
      };

      const formatDescription = description => {
        const data = jQuery('<div>' + description + '</div>').text().split(' ');
        return data.length > 20 ? data.slice(0, 30).join(' ') + '...' : data.join(' ');
      }

      const formatSentiment = sentiment => {
        const style = 'margin: 0 10px; font-weight: 400; color: #000; text-align: center; width: 62px; height: 22px; border-radius: 8px;';
        if (sentiment === 'neutral') return 'background: #8c8c8e;' + style;
        if (sentiment === 'positive') return 'background: #21ce99;' + style;
        if (sentiment === 'negative') return 'background: #f45531;' + style;
        return 'background: #000; color: #FFF !important;' + style;
      };

      const renderItem = item => {
        const html = `
          <div style="${style.item}" id="${item.guid}">
            <div style="${style.header}">
              <a class="robinews" target="__blank" href="${item.link}" style="${style.title}">${item.title} - <span style="${style.source}">${item.source.toUpperCase()}</span></a>
              <p style="${style.date}">${formatDate(item.pubDate)}</p>
              <p class="sentiment" style="${formatSentiment(item.sentiment)}">${item.sentiment}</p>
            </div>
            <div style="${style.description}">
              ${item.content && item.content !== 'n/a' ? formatDescription(item.content) : formatDescription(item.contentSnippet)}
            </div>
          </div>
        `;
        return html;
      };

      const renderLabel = () => {
        const html = `
          <div style="${style.label}">
            Live Feed
          </div>
        `;
        return html;
      }

      const renderItems = data => {
        const itemsHtml = data.map(item => renderItem(item));
        const html = `
          <div id="live-feed">
            <div id="${stockId}"></div>
            ${renderLabel()}
            <div style="${style.container}">
              <div style="${style.containerScroll}">
                ${itemsHtml.join('')}
              </div>
            </div>
          </div>
        `;
        return html;
      };

      const render = data => {
        const html = renderItems(data);
        jQuery('section').eq(3).prepend(html);
      };

      if (window.location.pathname !== '/') {
        taskRunner(parallel, stockId, stockName, (items) => {
          const stockIdLive = jQuery('.card-heading h3').first().text().split(' ')[1] ? jQuery('.card-heading h3').first().text().split(' ')[1].trim() : '';
          if (document.getElementById(stockIdLive) === null) {
            render(sortByDate(items));
            loadSentiment();
          }
        });
      }
    }
  };

  Robinews.Publisher.listeners.push(start);

})( window.Robinews = window.Robinews || {}, jQuery );
