document.addEventListener('DOMContentLoaded', () => {
chrome.tabs.query({ currentWindow: true, active: true }, (tabs) => {
const tabLocation = tabs[0].url;

(( Robinews, jQuery ) => {

  async function loadRiskPerf(stockId) {
    jQuery('#risk').html('');

    const data = await fetch('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/risk?stockId=' + stockId);
    const json = await data.json();

    const rating = json.data.response.docs[0].CurrentRating;
    const grade = json.data.response.docs[0].LetterGradeRating;

    const color = rating === 'Buy' ? '#21ce99' : '#f45531';
    const ratingStyle = rating === 'Buy' ? 'background-color: #21ce99; color: #fff; border-radius: 15px; padding: 2px 10px;' : 'background-color: #f45531; color: #fff; border-radius: 15px; padding: 2px 10px;';
    jQuery('#risk').append('<a style="color: #00e6ff; font-size: 10px; position: absolute; right: 35px;" href="https://www.thestreet.com/">By TheStreet</a>');
    jQuery('#risk').append('<div style="line-height: 26px; color: ' + color + '">Current Rating: <span style="' + ratingStyle + '">' + rating + '</span></div>');
    jQuery('#risk').append('<div style="line-height: 26px; color: ' + color + '">Letter Grade Rating: ' + grade + '</div>');
  };

  if (tabLocation.includes('stock')) {
    loadRiskPerf(tabLocation.replace('https://robinhood.com/stocks/', ''));
  }
  Robinews.loadRiskPerf = loadRiskPerf;

})( window.Robinews = window.Robinews || {}, jQuery );


});
});