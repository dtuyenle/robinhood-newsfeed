document.addEventListener('DOMContentLoaded', () => {
chrome.tabs.query({ currentWindow: true, active: true }, (tabs) => {
const tabLocation = tabs[0].url;

(( Robinews, jQuery ) => {

  async function loadSearch() {

    const stockData = await fetch('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/stock?stockId=aapl');
    const stockJson = await stockData.json();

    const options = {
      data: stockJson.data.map(stock => stock.symbol + '-' + stock.description),
      list: {
        match: {
          enabled: true
        },
        onChooseEvent: function() {
          var stockId = $("#autocomplete").getSelectedItemData().split('-')[0].trim().toLowerCase();

          jQuery('#risk').html('');
          jQuery('#statistic').html('');
          jQuery('#analyst').html('');
          jQuery('#insider').html('');
          jQuery('#headline').html('');
          jQuery('#news').html('');

          Robinews.loadAnalyst(stockId);
          Robinews.loadHeadline(stockId);
          Robinews.loadInsider(stockId);
          Robinews.loadNews(stockId);
          Robinews.loadPremium(stockId);
          Robinews.loadRiskPerf(stockId);
          Robinews.loadStat(stockId);
        }
      }
    };

    jQuery("#autocomplete").easyAutocomplete(options);

  };


  loadSearch();

})( window.Robinews = window.Robinews || {}, jQuery );


});
});