document.addEventListener('DOMContentLoaded', () => {
chrome.tabs.query({ currentWindow: true, active: true }, (tabs) => {
const tabLocation = tabs[0].url;


(( Robinews, jQuery ) => {

  const loadAnalyst = async (stockId) => {

      const data = await fetch('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/analyst?stockId=' + stockId);
      const json = await data.json();

      const style = `
        <style>
          #robinews-analyst .robinews-analyst_label {
            ${(Robinews.isOpen() ? 'color: #000' : 'color: #FFF')};
            ${(Robinews.isOpen() ? 'border-bottom: 1px solid rgb(227, 233, 237)' : 'border-bottom: 1px solid rgba(48,54,58,1)')};
            font-weight: 500;
            font-size: 25px;
            padding: 16px 2px;
            margin: 35px 0;
          }

          #robinews-analyst .robinews-analyst_label a {
            color: #00e6ff;
            font-size: 12px;
          }

          #robinews-analyst .robinews-analyst-content {
            font-size: 14px;
            margin-bottom: 40px;
            background: rgba(30,34,37,1);
            color: #ccc;
            padding: 15px 18px;
            line-height: 1.5;
            position: relative;
            border-radius: 6px;
            border: 1px solid rgba(64,73,78,1);
          }

          #robinews-analyst .robinews-analyst-content strong {
            color: #fff;
          }

          #robinews-analyst .robinews-analyst-content .source {
            color: #00e6ff;
            font-size: 10px;
            position: absolute;
            right: 10px;
            bottom: 10px;
          }

          #robinews-analyst .robinews-analyst-content .source a {
            color: #00e6ff;
          }

          #robinews-analyst .robinews-analyst-content .client-components-stock-research-analysts-price-target-style__hold {
            color: #fff;
            font-size: 17px;
          }

          #robinews-analyst .robinews-analyst-content .client-components-stock-research-analysts-price-target-style__high {
            color: #21ce99;
            font-size: 17px;
          }

          #robinews-analyst .robinews-analyst-content .client-components-stock-research-analysts-price-target-style__low {
            color: rgb(255,80,0);
            font-size: 17px;
          }

          #robinews-analyst .robinews-analyst-content .robinews-concensus {
            color: rgb(0,200,5);
            margin: 8px 0 0 0;
            font-size: 17px;
            font-weight: bold;
          }
        </style>
      `;

      const html = data => {
        return `
          <div class="robinews-${stockId}" id="robinews-analyst">
            ${style}
            <div class="robinews-analyst_label">
              Analyst Concensus <a href="https://www.robinews.net/"> By Robinews</a>
            </div>
            <div class="robinews-analyst-content">
              <span class="source"><a target="__blank" href="https://www.tipranks.com/">Source tipranks</a></span>
              ${data}
            </div>
          </div>
        `;
      };

      if (json.data && json.data.length > 0) {
        jQuery('#analyst').html(html(json.data)) ;
      }
  };

  if (tabLocation.includes('stock')) {
    loadAnalyst(tabLocation.replace('https://robinhood.com/stocks/', ''));
  }
  Robinews.loadAnalyst = loadAnalyst;

})( window.Robinews = window.Robinews || {}, jQuery );


});
});