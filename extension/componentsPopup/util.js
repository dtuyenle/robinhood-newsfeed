(( Robinews, jQuery ) => {

  Robinews.cleanup = (str) => {
    let string = str.trim().toLowerCase();
    string = string.replace(/(\.|\,|\!)/g, '');
    string = string.replace('undefined', '');
    string = string.replace(/\/n/g, '');
    string = string.split(' ').filter(char => char !== '');
    return string;
  };


})( window.Robinews = window.Robinews || {}, jQuery );
