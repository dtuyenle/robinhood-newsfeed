document.addEventListener('DOMContentLoaded', () => {
chrome.tabs.query({ currentWindow: true, active: true }, (tabs) => {
const tabLocation = tabs[0].url;


(( Robinews, jQuery ) => {
  const loadNews = (stockId) => {

    async function taskRunner(stockId, callback) {
      const url = 'https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/news';
      const result = await fetch(url + '?stockId=' + stockId).then(res => res.json());
      callback(result.data);
    }

    const sortByDate = (array) => {
      array.sort((a, b) => {
        return new Date(b.pubDate) - new Date(a.pubDate);
      });
      return array;
    };

    const formatDate = date => {
      return moment(date).fromNow().replace(' ago', '').replace('days', 'd').replace('hours', 'h');
    };

    const formatDescription = description => {
      const data = jQuery('<div>' + description + '</div>').text().split(' ');
      return data.length > 20 ? data.slice(0, 30).join(' ') + '...' : data.join(' ');
    }

    const style = `
    <style>
      #live-feed .news_label {
        ${(Robinews.isOpen() ? 'color: #000' : 'color: #FFF')};
        ${(Robinews.isOpen() ? 'border-bottom: 1px solid rgb(227, 233, 237)' : 'border-bottom: 1px solid rgba(48,54,58,1)')};
        font-weight: 500;
        font-size: 25px;
        padding: 16px 2px;
        margin: 35px 0;
      }

      #live-feed .news_label a {
        color: #00e6ff;
        font-size: 12px;
      }

      #live-feed .robinews-news-container {
        height: 500px;
        width: 100%;
        overflow-y: auto;
        overflow-x: hidden;
        margin-bottom: 60px;
      }

      #live-feed-content {
        width: 100%;
        height: 100%
        overflow-y: scroll;
        padding-right: 17px;
        box-sizing: content-box;
      }

      .robinews-news-item {
        padding: 10px 0;
        margin: 8px 0;
      }

      .robinews-news-item-header {
        display: flex;
        justify-content: space-between;
      }

      .robinews-news-item-title {
        // color: #21ce99 !important;
        ${Robinews.isOpen() ? 'color: #000 !important;' : 'color: #fff !important;'}
        margin: 0 0 5px;
        width: 500px;
        font-size: 15px;
        font-weight: 500 !important;
      }

      .robinews-news-item-date {
        ${Robinews.isOpen() ? 'color: #666' : 'color: #8c8c8e'};
        margin: 0 0 5px;
      }

      .robinews-news-item-description {
        ${(Robinews.isOpen() ? 'color: #666;' : 'color: rgba(121,133,139,1);')}
        font-size: 14px;
      }

      .robinews-news-item-source {
        ${(Robinews.isOpen() ? 'color: #666' : 'color: #fff')};
      }

      .robinews-news-item-sentiment {
        margin: 0 10px;
        font-weight: 400;
        color: #000;
        text-align: center;
        width: 62px;
        height: 22px;
        border-radius: 15px;
      }
    </style>
  `;

    const formatSentiment = sentiment => {
      if (sentiment === 'neutral') return 'background: #8c8c8e;';
      if (sentiment === 'positive') return 'background: #21ce99;';
      if (sentiment === 'negative') return 'background: #f45531;';
      return 'background: #000; color: #FFF !important;';
    };

    const renderItem = item => {
      const html = `
        <div class="robinews-news-item" id="${item.guid}">
          <p class="robinews-news-item-date">
            <span class="robinews-news-item-source">
              ${item.source.toUpperCase()}
            </span> - ${formatDate(item.pubDate)}
          </p>
          <div class="robinews-news-item-header">
            <a class="robinews-news-item-title" target="__blank" href="${item.link}">
              ${item.title}
            </a>
            <p class="robinews-news-item-sentiment" style="${formatSentiment(item.sentiment)}">${item.sentiment}</p>
          </div>
          <div class="robinews-news-item-description">
            ${item.content && item.content !== 'n/a' ? formatDescription(item.content) : formatDescription(item.contentSnippet)}
          </div>
        </div>
      `;
      return html;
    };

    const renderItems = data => {
      const itemsHtml = data.map(item => renderItem(item));
      const html = `
        <div id="live-feed" class="robinews-${stockId}">
          ${style}
          <div id="${stockId}"></div>
          <div class="news_label">
            Live News Feed <a target="__blank" href="https://www.robinews.net/"> By Robinews</a>
          </div>
          <div class="robinews-news-container">
            <div id="live-feed-content">
              ${itemsHtml.join('')}
            </div>
          </div>
        </div>
      `;
      return html;
    };

    const render = data => {
      const html = renderItems(data);
      jQuery('#news').html(html);
    };

    taskRunner(stockId, (items) => {
      render(sortByDate(items));
    });
  }

  if (tabLocation.includes('stock')) {
    loadNews(tabLocation.replace('https://robinhood.com/stocks/', ''));
  } else {
    loadNews(tabLocation.replace('stock news'));
  }
  Robinews.loadNews = loadNews;

})( window.Robinews = window.Robinews || {}, jQuery );



});
});