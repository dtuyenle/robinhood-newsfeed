(( Robinews, jQuery ) => {

  Robinews.cleanup = (str) => {
    let string = str.trim().toLowerCase();
    string = string.replace(/(\.|\,|\!)/g, '');
    string = string.replace('undefined', '');
    string = string.replace(/\/n/g, '');
    string = string.split(' ').filter(char => char !== '');
    return string;
  };


  Robinews.escapeHtml = (unsafe) => {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
   }

})( window.Robinews = window.Robinews || {}, jQuery );
