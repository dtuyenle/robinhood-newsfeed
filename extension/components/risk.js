(( Robinews, jQuery ) => {

  async function loadRiskPerf() {
    if (Robinews.isStockPage()) {

      jQuery('#risk').remove();

      const stockId = Robinews.getStockId();
      const data = await fetch('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/risk?stockId=' + stockId);
      const json = await data.json();

      const rating = json.data.response.docs[0].CurrentRating;
      const grade = json.data.response.docs[0].LetterGradeRating;

      const color = rating === 'Buy' ? '#21ce99' : '#f45531';
      const ratingStyle = rating === 'Buy' ? 'background-color: #21ce99; color: #fff; border-radius: 15px; padding: 2px 10px;' : 'background-color: #f45531; color: #fff; border-radius: 15px; padding: 2px 10px;';
      jQuery('.sidebar-content').find('form').first().parent().append('<div style="width: 100%; border-top: 1px solid var(--rh__neutral-bg3); padding: 10px 25px;" id="risk"></div>');
      jQuery('#risk').append('<a style="color: #00e6ff; font-size: 10px; position: absolute; right: 35px;" href="https://www.thestreet.com/">Source TheStreet</a>');
      jQuery('#risk').append('<div style="line-height: 26px; color: ' + color + '">Current Rating: <span style="' + ratingStyle + '">' + rating + '</span></div>');
      jQuery('#risk').append('<div style="line-height: 26px; color: ' + color + '">Letter Grade Rating: ' + grade + '</div>');
    }
  };

  Robinews.Publisher.listeners.push(loadRiskPerf);

})( window.Robinews = window.Robinews || {}, jQuery );
