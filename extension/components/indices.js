(( Robinews, jQuery ) => {


  async function loadIndices() {

    const css = `
      <style>
        #robinews-indices {
          display: flex;
          justify-content: flex-start;
          padding: 2px;
          margin-bottom: 15px;
        }

        .robinews-indices-item {
          margin-right: 10px;
        }

        .robinews-indices-positive {
          color: rgb(0,200,5);
        }

        .robinews-indices-negative {
          color: rgb(255,80,0);
        }

        .robinews-indices-name {
          ${(Robinews.isOpen() ? 'color: #000 !important' : 'color: #fff !important')};
        }

        .robinews-by {
          font-size: 10px;
          line-height: 25px;
        }

        .robinews-by a {
          color: #00e6ff !important;
        }

      </style>
    `;

    jQuery('#robinews-indices').remove();
    jQuery('body').append(css);

    const load = async () => {
      if (Robinews.isStockPage() && jQuery('.main-container')) {

        const stockId = Robinews.getStockId();
        const data = await fetch('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/indices?stockId=' + stockId);
        const json = await data.json();

        const sp500 = json.data.filter(item => item.id === 'sp500')[0];

        const sp500Close = sp500.jsonResponse.chart.result[0].meta.previousClose;
        const sp500Current = sp500.jsonResponse.chart.result[0].indicators.quote[0].high.filter(item => item)[sp500.jsonResponse.chart.result[0].indicators.quote[0].high.filter(item => item).length - 1];
        const sp500Change = sp500Current - sp500Close;
        const sp500ChangeLabel = sp500Change > 0 ? '+' + Math.abs(sp500Change) : '-' + Math.abs(sp500Change);
        const sp500ChangePercent = (((sp500Current - sp500Close) * 100)/sp500Close).toFixed(2);
        const sp500ChangePercentLabel = sp500ChangePercent > 0 ? '+' + Math.abs(sp500ChangePercent) : '-' + Math.abs(sp500ChangePercent);
        const sp500Positive = sp500Change > 0;

        const dow = json.data.filter(item => item.id === 'dow')[0];
        const dowClose = dow.jsonResponse.chart.result[0].meta.previousClose;
        const dowCurrent = dow.jsonResponse.chart.result[0].indicators.quote[0].high.filter(item => item)[dow.jsonResponse.chart.result[0].indicators.quote[0].high.filter(item => item).length - 1];
        const dowChange = dowCurrent - dowClose;
        const dowChangeLabel = dowChange > 0 ? '+' + Math.abs(dowChange) : '-' + Math.abs(dowChange);
        const dowChangePercent = (((dowCurrent - dowClose) * 100)/dowClose).toFixed(2);
        const dowChangePercentLabel = dowChangePercent > 0 ? '+' + Math.abs(dowChangePercent) : '-' + Math.abs(dowChangePercent);
        const dowPositive = dowChange > 0;

        const nasdaq = json.data.filter(item => item.id === 'nasdaq')[0];
        const nasdaqClose = nasdaq.jsonResponse.chart.result[0].meta.previousClose;
        const nasdaqCurrent = nasdaq.jsonResponse.chart.result[0].indicators.quote[0].high.filter(item => item)[nasdaq.jsonResponse.chart.result[0].indicators.quote[0].high.filter(item => item).length - 1];
        const nasdaqChange = nasdaqCurrent - nasdaqClose;
        const nasdaqChangeLabel = nasdaqChange > 0 ? '+' + Math.abs(nasdaqChange) : '-' + Math.abs(nasdaqChange);
        const nasdaqChangePercent = (((nasdaqCurrent - nasdaqClose) * 100)/nasdaqClose).toFixed(2);
        const nasdaqChangePercentLabel = nasdaqChangePercent > 0 ? '+' + Math.abs(nasdaqChangePercent) : '-' + Math.abs(nasdaqChangePercent);
        const nasdaqPositive = nasdaqChange > 0;


        jQuery('#robinews-indices').remove();
        jQuery('.main-container').prepend(`
          <div id="robinews-indices">
            <div class="robinews-indices-item">
              <a class="robinews-indices-name" href="https://finance.yahoo.com/quote/ES%3DF?p=ES%3DF" target="__blank">
                <span>S&P Futures</span>
              </a>
              <span class="${sp500Positive ? 'robinews-indices-positive' : 'robinews-indices-negative'}">
                ${sp500ChangeLabel} (${sp500ChangePercentLabel}%)
              </span>
            </div>
            <div class="robinews-indices-item">
              <a class="robinews-indices-name" href="https://finance.yahoo.com/quote/YM%3DF?p=YM%3DF" target="__blank">
                <span>Dow Futures</span>
              </a>
              <span class="${dowPositive ? 'robinews-indices-positive' : 'robinews-indices-negative'}">
                ${dowChangeLabel} (${dowChangePercentLabel}%)
              </span>
            </div>
            <div class="robinews-indices-item">
              <a class="robinews-indices-name" href="https://finance.yahoo.com/quote/NQ%3DF?p=NQ%3DF" target="__blank">
                <span>Nasdaq Futures</span>
              </a>
              <span class="${nasdaqPositive ? 'robinews-indices-positive' : 'robinews-indices-negative'}">
                ${nasdaqChangeLabel} (${nasdaqChangePercentLabel}%)
              </span>
            </div>
            <div class="robinews-by"><a target="__blank" href="https://www.robinews.net/"> By Robinews</a></div>
          </div>
        `);
      }
    };

    load();

  };

  Robinews.Publisher.listeners.push(loadIndices);

})( window.Robinews = window.Robinews || {}, jQuery );
