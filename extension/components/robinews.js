(( Robinews, jQuery ) => {
  // Section
  Robinews.Section = {
    news: '#live-feed',
    isNewsLoaded: jQuery('#live-feed').length > 0,
    profit: '#profit-indicator',
    isProfitLoaded: jQuery('#profit-indicator').length > 0,
    risk: '#risk',
    isRiskLoaded: jQuery('#risk').length > 0,
    sector: '#sector-indicator',
    isSectorLoaded: jQuery('#sector-indicator').length > 0,
  };

  // Check which page currently on
  Robinews.isMainPage = () => window.location.pathname === '/';
  Robinews.isStockPage = () => window.location.pathname.includes('stocks');

  // Global publisher
  Robinews.Publisher = {
    listeners: [],
    broadcast: () => {
      Robinews.Publisher.listeners.forEach((listener) => {
        listener();
      });
    }
  };

  // Check if market is opened
  Robinews.isOpen = () => !jQuery('body').attr('class').includes('is-dark-theme');
  // Robinews.isOpen = () => jQuery('body').attr('class').includes('is-dark-theme');

  // Get stock id from url
  Robinews.getStockId = () => {
    const correctPage = window.location.pathname.includes('stocks');
    const stockId = window.location.pathname.replace('/stocks/', '');
    const check = correctPage && stockId && stockId.length > 0;
    return check ? stockId : 'stock market';
  };

  // Clean up for new load
  Robinews.cleanup = () => {
    const stockIdLive = Robinews.getStockId();
    if (stockIdLive !== '') {
      jQuery(Robinews.Section.news).remove();
      jQuery('#' + stockIdLive).remove();
    }
    jQuery('#robinews-analyst').remove();
    jQuery('#robinews-stat').remove();
    jQuery(Robinews.Section.risk).remove();
    jQuery(Robinews.Section.sector).remove();
  };

  // Indicator to see if user navigate to a new page
  Robinews.prevPath = Robinews.getStockId();

  // Check for page loaded jQuery('section').eq(3)


  let initialRun = false;
  // Initial load
  setTimeout(() => {
    Robinews.Publisher.broadcast();
    initialRun = true;

    // Run every 2 seconds to check if user navigate to a new page
    setInterval(() => {
      if (!initialRun) {
        Robinews.Publisher.broadcast();
      }
      const correctPage = Robinews.isMainPage() || Robinews.isStockPage();
      const currPath = Robinews.getStockId();

      if (currPath !== Robinews.prevPath && correctPage) {
        Robinews.cleanup();
        Robinews.prevPath = currPath;

        // tracking
        // mixpanel.track('Page View', currPath);
        // mixpanel.track_links('a.robinews', 'Stock News View', {
        //   'stockId': currPath,
        //   'referrer': document.referrer
        // });

        setTimeout(() => {
          Robinews.Publisher.broadcast();
        }, 1000);
      }
    }, 2000);
  }, 4000);

})( window.Robinews = window.Robinews || {}, jQuery );
