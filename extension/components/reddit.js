(( Robinews, jQuery ) => {
  const escapeHtml = (unsafe) => {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
  };

  const start = () => {
    if (Robinews.isStockPage()) {

      const stockId = Robinews.getStockId();
      const stockName = jQuery('h1').first().text();

      async function taskRunner(stockId, callback) {
        const url = 'https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/reddit';
        const result = await fetch(url + '?stockId=' + stockId + '&stockName=' + stockName).then(res => res.json());
        callback(result);
      }

      const formatDate = date => {
        return moment.utc(date).format('MM-DD-YYYY');
      };


      const style = `
      <style>
        #robinews-reddit .reddit_label {
          ${(Robinews.isOpen() ? 'color: #000' : 'color: #FFF')};
          ${(Robinews.isOpen() ? 'border-bottom: 1px solid rgb(227, 233, 237)' : 'border-bottom: 1px solid rgba(48,54,58,1)')};
          font-weight: 500;
          font-size: 25px;
          padding: 16px 2px;
          margin: 35px 0;
        }

        #robinews-reddit .reddit_label a {
          color: #00e6ff;
          font-size: 12px;
        }

        #robinews-reddit .robinews-reddit-container {
          height: 500px;
          width: 100%;
          overflow-y: auto;
          overflow-x: hidden;
          margin-bottom: 60px;
        }

        #reddit-content {
          width: 100%;
          height: 100%;
          word-break: break-all;
          overflow-y: scroll;
          padding-right: 17px;
          box-sizing: content-box;
        }

        .robinews-reddit-item {
          padding: 10px 0;
          margin: 8px 0;
        }

        .robinews-reddit-item-header {
          display: flex;
          justify-content: space-between;
        }

        .robinews-reddit-item-title {
          // color: #21ce99 !important;
          ${Robinews.isOpen() ? 'color: #000 !important;' : 'color: #fff !important;'}
          margin: 0 0 5px;
          width: 500px;
          font-size: 15px;
          font-weight: 900 !important;
        }

        .robinews-reddit-item-date {
          ${Robinews.isOpen() ? 'color: #666' : 'color: #8c8c8e'};
          margin: 0 0 5px;
        }

        .robinews-reddit-item-description {
          ${(Robinews.isOpen() ? 'color: #666;' : 'color: rgba(121,133,139,1);')}
          font-size: 14px;
        }

        .robinews-reddit-item-source {
          ${(Robinews.isOpen() ? 'color: #666' : 'color: #fff')};
        }

      </style>
    `;

      const renderItem = item => {
        const html = `
          <div class="robinews-reddit-item">
            <p class="robinews-reddit-item-date">
              ${formatDate(item.pubDate)}
            </p>
            <div class="robinews-reddit-item-header">
              <a class="robinews-reddit-item-title" target="__blank" href="${item.url}">
                ${item.title}
              </a>
            </div>
            <div class="robinews-reddit-item-description">
              ${jQuery('<div>' + item.text + '</div>').text()}
            </div>
          </div>
        `;
        return html;
      };

      const renderItems = data => {
        const itemsHtml = data.map(item => renderItem(item));
        const html = `
          <div id="robinews-reddit">
            ${style}
            <div id="${stockId}"></div>
            <div class="reddit_label">
              Reddit Discussion <a target="__blank" href="https://www.robinews.net/"> By Robinews</a>
            </div>
            <div class="robinews-reddit-container">
              <div id="reddit-content">
                ${itemsHtml.join('')}
              </div>
            </div>
          </div>
        `;
        return html;
      };

      const render = data => {
        const html = renderItems(data);
        jQuery('section[data-testid="ChartSection"]').next().next().next().prepend(html);
      };

      if (window.location.pathname !== '/') {
        taskRunner(stockId, (data) => {
          const items = data.data;
          const stockIdLive = jQuery('.card-heading h3').first().text().split(' ')[1] ? jQuery('.card-heading h3').first().text().split(' ')[1].trim() : '';
          if (document.getElementById(stockIdLive) === null) {
            if (items && items.length > 0 && data.stockId === Robinews.getStockId()) {
              render(items);
            }
          }
        });
      }
    }
  };

  Robinews.Publisher.listeners.push(start);

})( window.Robinews = window.Robinews || {}, jQuery );
