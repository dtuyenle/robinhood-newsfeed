(( Robinews, jQuery ) => {

  async function loadStatistic() {
    if (Robinews.isStockPage()) {

      jQuery('#robinews-stat').remove();

      const stockId = Robinews.getStockId();
      const data = await fetch('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/statistic?stockId=' + stockId);
      const json = await data.json();

      const style = `
        <style>
          #robinews-stat {
            display: none;
            position: relative;
            margin-bottom: 50px;
          }

          #robinews-stat .robinews-stat-content {
            ${(Robinews.isOpen() ? 'color: #000' : 'color: rgba(121,133,139,1)')};
          }

          #robinews-stat table td {
            padding-right: 20px;
          }

          #robinews-stat .robinews-stat-label {
            ${(Robinews.isOpen() ? 'color: #000' : 'color: #FFF')};
            ${(Robinews.isOpen() ? 'border-bottom: 1px solid rgb(227, 233, 237)' : 'border-bottom: 1px solid rgba(48,54,58,1)')};
            font-weight: 500;
            font-size: 25px;
            padding: 16px 2px;
            margin: 35px 0;
            margin-bottom: 20px;
          }

          #robinews-stat .robinews-stat-label a {
            color: #00e6ff;
            font-size: 12px;
          }

          #robinews-stat h3 {
            font-size: 18px;
            margin: 15px 0;
            ${(Robinews.isOpen() ? 'color: #000' : 'color: #FFF')};
          }

          #robinews-stat section div:nth-of-type(3) {
            display: flex;
            justify-content: space-between;
          }

          .robinews-stat-source {
            position: absolute;
            right: 0;
          }

          .robinews-stat-source a {
            color: #00e6ff !important;
            font-size: 10px;
          }

        </style>
      `;

      const html = data => {
        return `
          <div class="robinews-${stockId}" id="robinews-stat">
            ${style}
            <div class="robinews-stat-label">
              Financial Statistic <a target="__blank" href="https://www.robinews.net/"> By Robinews</a>
            </div>
            <div class="robinews-stat-content">
              ${data}
            </div>
            <div class="robinews-stat-source">
              <a target="__blank" href="https://finance.yahoo.com/">Source Yahoo Finance</a>
            </div>
          </div>
        `;
      };


      if (json.data && json.data.length > 0 && json.stockId === Robinews.getStockId()) {
        jQuery('section[data-testid="ChartSection"]').next().next().next().prepend(html(json.data));

        jQuery('#robinews-stat .source').remove();

        jQuery('#robinews-stat section div').eq(0).hide();
        jQuery('#robinews-stat section div').eq(1).hide();
        jQuery('#robinews-stat section div').eq(2).find('h2').hide();
        jQuery('#robinews-stat section div').eq(2).find('div').eq(0).hide();

        jQuery('#robinews-stat').show();

      }
    }
  };

  Robinews.Publisher.listeners.push(loadStatistic);

})( window.Robinews = window.Robinews || {}, jQuery );
