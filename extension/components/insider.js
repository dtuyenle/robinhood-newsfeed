(( Robinews, jQuery ) => {

  async function loadInsider() {
    if (Robinews.isStockPage()) {

      jQuery('#robinews-insider').remove();

      const stockId = Robinews.getStockId();
      const data = await fetch('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/insider?stockId=' + stockId);
      const json = await data.json();

      const style = `
        <style>
          #robinews-insider  {
            margin-bottom: 50px;
          }

          #robinews-insider .insider_label {
            ${(Robinews.isOpen() ? 'color: #000' : 'color: #FFF')};
            ${(Robinews.isOpen() ? 'border-bottom: 1px solid rgb(227, 233, 237)' : 'border-bottom: 1px solid rgba(48,54,58,1)')};
            font-weight: 500;
            font-size: 25px;
            padding: 16px 2px;
            margin: 35px 0;
          }

          #robinews-insider .insider_label a {
            color: #00e6ff;
            font-size: 12px;
          }

          #robinews-insider .insider_content {
            max-height: 400px;
            overflow-y: auto;
            ${(Robinews.isOpen() ? 'border-bottom: 1px solid #000' : 'border-bottom: 1px solid rgba(64,73,78,1)')};
          }

          #robinews-insider .insider_content table {
            border-collapse: collapse;
            width: 100%;
          }

          #robinews-insider .insider_content table th {
            background: rgba(30,34,37,1);
            color: #fff;
            border: 1px solid rgba(64,73,78,1);
            font-size: 14px;
            padding: 10px;
            text-align: left;
          }

          #robinews-insider .insider_content table .insider_content_header th {
            ${(Robinews.isOpen() ? 'border: 1px solid #000' : 'border: 1px solid #fff')};
          }

          #robinews-insider .insider_content table tr:hover {
            background: #ccc;
            cursor: pointer;
          }

          #robinews-insider .insider_content table tr:last-child td {
            border-bottom: none;
          }

          #robinews-insider .insider_content table td {
            ${(Robinews.isOpen() ? 'color: #666;' : 'color: rgba(121,133,139,1);')}
            border: 1px solid rgba(64,73,78,1);
            padding: 2px 8px;
            text-align: left;
          }

          #robinews-insider .insider_content table td a {
            color: inherit;
          }
        </style>
      `;

      const itemHtml = item => `
        <tr>
          <td>
            <a target="_blank" href="${item.form}">${item.name}</a>
          </td>
          <td>
            <a target="_blank" href="${item.form}">${item.title}</a>
          </td>
          <td style="width: 90px;">
            <a target="_blank" href="${item.form}">${item.date}</a>
          </td>
          <td style="${item.action.includes('Sell') ? 'color: #f45531;' : 'color: #21ce99;'}">
            <a target="_blank" href="${item.form}">${item.action.replace('Automatic ', '')}</a>
          </td>
          <td>
            <a target="_blank" href="${item.form}">${item.price}</a>
          </td>
          <td>
            <a target="_blank" href="${item.form}">${item.share}</a>
          </td>
          <td>
            <a target="_blank" href="${item.form}">${item.holding}</a>
          </td>
        </tr>
      `;

      const html = data => `
        <div id="robinews-insider" class="robinews-${stockId}">
          ${style}
          <div class="insider_label">
            Insider Activity <a target="__blank" href="https://www.robinews.net/"> By Robinews</a>
          </div>
          <div class="insider_content">
            <table>
              <tr class="insider_content_header">
                <th>Name</th>
                <th>Title</th>
                <th>Date</th>
                <th>Action</th>
                <th>Price</th>
                <th>Share</th>
                <th>Holding</th>
              </tr>

              ${data.map(item => {
                return itemHtml(item);
              }).join('')}
            </table>
          </div>
        </div>
      `;

      if (json.data && json.data.length > 0 && json.stockId === Robinews.getStockId()) {
        jQuery('section[data-testid="ChartSection"]').next().next().next().prepend(html(json.data));
      }
    }
  };

  Robinews.Publisher.listeners.push(loadInsider);

})( window.Robinews = window.Robinews || {}, jQuery );
