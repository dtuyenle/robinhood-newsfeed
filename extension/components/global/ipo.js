(( Robinews, jQuery ) => {

  const css = `
    <style>
      #robinews-ipo h3 {
        background: #fff;
        padding: 10px;
        color: #000;
        font-size: 18px;
        border: 1px solid #000;
      }

      #robinews-ipo h3 a {
        color: #00e6ff;
        font-size: 10px;
        text-decoration: none;
      }

      .robinews-ipo-stock {
        border: 1px solid #CCC;
        margin: 3px 2px;
        padding: 0 5px;
        border-radius: 20px;
        background: #000;
      }

      .robinews-ipo-stock:hover {
        background: #d6d6d6;
      }

      .robinews-ipo-stock:hover a {
        color: #000;
      }

      .robinews-ipo-stock a {
        text-decoration: none;
        color: #fff;
        font-size: 11px;
      }

      .robinews-ipo-content {
        display: flex;
        justify-content: flex-start;
        flex-wrap: wrap;
      }

      .robinews-ipo-date {
        color: #000;
      }
    </style>
  `;

  const loadIpo = async () => {

    const ipoUrl = 'https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/ipo';
    const ipoResponse = await fetch(ipoUrl);
    const ipoData = await ipoResponse.json();

    const getIpoHtml = () => {
      return Object.keys(ipoData.data)
      .reverse()
      .map((key, index) => {
        const date = moment(key).format('MMMM Do YYYY');
        const ipoItems = ipoData.data[key];

        return `
          <div class="accordion ${index === 0 ? 'active' : ''}">
            <div class="accordion_tab robinews-ipo-date">
              ${date}
              <div class="accordion_arrow">
                <img src="https://i.imgur.com/PJRz0Fc.png" alt="arrow">
              </div>
            </div>
            <div class="accordion_content robinews-ipo-content">
              ${ipoItems.map(ipoItem => `
                <div class="robinews-ipo-stock">
                  <a target="_blank" href="https://www.nasdaq.com/market-activity/stocks/${ipoItem.proposedTickerSymbol}">${ipoItem.companyName}</a>
                </div>
              `).join('')}
            </div>
          </div>
        `;
      }).join('');
    };

    const dataHtml = getIpoHtml();

    const html = `
      <div id="robinews-ipo">
        <h3>IPO Calendar <a target="__blank" href="https://www.robinews.net/"> By Robinews</a></h3>
        <div class="wrapper">
          ${dataHtml}
        </div>
      </div>
    `;

    jQuery('#robinews-content-ipo').append(css);
    jQuery('#robinews-content-ipo').append(html);
  };

  setTimeout(() => {
    loadIpo();
  }, 2000);

})( window.Robinews = window.Robinews || {}, jQuery );
