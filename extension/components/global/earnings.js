(( Robinews, jQuery ) => {

  const css = `
    <style>
      #robinews-earnings h3 {
        background: #fff;
        padding: 10px;
        color: #000;
        font-size: 18px;
        margin-top: 50px;
        border: 1px solid #000;
      }

      #robinews-earnings h3 a {
        color: #00e6ff;
        font-size: 10px;
        text-decoration: none;
      }

      .robinews-earning-stock {
        border: 1px solid #CCC;
        margin: 3px 2px;
        padding: 0 5px;
        border-radius: 20px;
        background: #000;
      }

      .robinews-earning-stock:hover {
        background: #d6d6d6;
      }

      .robinews-earning-stock:hover a {
        color: #000;
      }

      .robinews-earning-stock a {
        text-decoration: none;
        color: #fff;
        font-size: 11px;
      }

      .robinews-earning-content {
        display: flex;
        justify-content: flex-start;
        flex-wrap: wrap;
      }

      .robinews-earning-date {
        color: #000;
      }
    </style>
  `;

  const getOneWeekDates = () => {
    const result = [];
    const currDate = new Date();
    for (let i = 0; i < 10; i++) {
      if (i > 0) {
        currDate.setDate(currDate.getDate()+1);
      }
      const formatedDate = moment(currDate).format('YYYY-MM-DD');
      result.push(formatedDate);
    }
    return result;
  }

  const loadEarnings = async () => {

    const dates = getOneWeekDates();
    const result = [];

    for(let i = 0; i < dates.length; i++) {
      const url = 'https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/earnings?date=' + dates[i];
      const response = await fetch(url);
      const data = await response.json();
      result.push({
        date: dates[i],
        data: data.data,
      });
    }

    const getWeekEarningHtml = () => {
      return result
      .filter(item => item.data)
      .map((item, index) => {
        const date = moment(item.date).format('MMMM Do YYYY');

        return `
          <div class="accordion ${index === 0 ? 'active' : ''}">
            <div class="accordion_tab robinews-earning-date">
              ${date}
              <div class="accordion_arrow">
                <img src="https://i.imgur.com/PJRz0Fc.png" alt="arrow">
              </div>
            </div>
            <div class="accordion_content robinews-earning-content">
              ${item.data.map(stockEarning => `
                <div class="robinews-earning-stock">
                  <a target="_blank" href="/stocks/${stockEarning.symbol}">${stockEarning.name}</a>
                </div>
              `).join('')}
            </div>
          </div>
        `;
      }).join('');
    };

    const dataHtml = getWeekEarningHtml();

    const html = `
      <div id="robinews-earnings">
        <h3>Earning Calendar <a target="__blank" href="https://www.robinews.net/"> By Robinews</a></h3>
        <div class="wrapper">
          ${dataHtml}
        </div>
      </div>
    `;

    jQuery('#robinews-content-earnings').append(css);
    jQuery('#robinews-content-earnings').append(html);
  };

  setTimeout(() => {
    loadEarnings();
  }, 2000);

})( window.Robinews = window.Robinews || {}, jQuery );
