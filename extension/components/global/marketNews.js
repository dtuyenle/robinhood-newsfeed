(( Robinews, jQuery ) => {
  const sortByDate = (array) => {
    array.sort((a, b) => {
      return new Date(b.pubDate) - new Date(a.pubDate);
    });
    return array;
  };

  const formatDate = date => {
    return moment(date).fromNow().replace(' ago', '').replace('days', 'd').replace('hours', 'h');
  };

  const formatDescription = description => {
    const data = jQuery('<div>' + description + '</div>').text().split(' ');
    return data.length > 20 ? data.slice(0, 30).join(' ') + '...' : data.join(' ');
  };

  const formatSentiment = sentiment => {
    if (sentiment === 'neutral') return 'background: #8c8c8e;';
    if (sentiment === 'positive') return 'background: #21ce99;';
    if (sentiment === 'negative') return 'background: #f45531;';
    return 'background: #000; color: #FFF !important;';
  };

  const css = `
    <style>
    #robinews-top-news {
      margin: 0 20px;
    }

    #robinews-top-news h3 a {
      color: #00e6ff;
      font-size: 10px;
    }

    #robinews-top-news a {
      text-decoration: none;
    }

    #robinews-top-news h3 {
      background: #fff;
      color: #000;
      padding: 10px;
      font-size: 19px;
      border: 1px solid #000;
    }

    .robinews-top-news-container {
      padding: 25px 0;
    }

    .robinews-top-news-item {
      display: flex;
      background: #fff;
    }

    .robinews-top-news-item-image {
    }

    .robinews-top-news-content {
      padding: 10px;
      color: rgb(111,120,126);
    }

    .robinews-top-news-content-title {
      color: #000;
      font-weight: 600;
    }

    .robinews-top-news-source-date {
      font-size: 11px;
    }

    .robinews-top-news-source-date span {
      color: #000;
    }

    .robinews-top-news-item-sentiment {
      padding-top: 25px;
    }

    </style>
  `;

  const loadMarketNews = async () => {
    const rssNewsUrl = 'https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/news';
    const rssNewsData = await fetch(rssNewsUrl + '?stockId=' + 'stock market').then(res => res.json());

    const premiumNewsUrl = 'https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/premiumNews?stockName=stock news&stockId=stock news';
    const premiumNewsData = await fetch(premiumNewsUrl).then(res => res.json());

    const premiumNewsUrl1 = 'https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/premiumNews?stockName=stock news&stockId=stock market';
    const premiumNewsData1 = await fetch(premiumNewsUrl1).then(res => res.json());

    const premiumNewsHtml = premiumNewsData.data.concat(premiumNewsData1.data).map(premiumNewsItem => (
      `
        <a href="${premiumNewsItem.url}">
          <div class="robinews-top-news-item">
            <div class="robinews-top-news-item-image">
              <img src="${premiumNewsItem.image}" />
            </div>
            <div class="robinews-top-news-content">
              <div class="robinews-top-news-source-date">
                <span>${premiumNewsItem.source}</span> - ${premiumNewsItem.date}
              </div>
              <div class="robinews-top-news-content-title">${premiumNewsItem.title}</div>
              <div>${premiumNewsItem.description}</div>
            </div>
          </div>
        </a>
      `
    )).join('');


    const rssNewsHtml = sortByDate(rssNewsData.data).slice(0, 55).map(rssNewsItem => (
      `
        <a href="${rssNewsItem.link}">
          <div class="robinews-top-news-item">
            <div class="robinews-top-news-item-sentiment">
              <p class="robinews-news-item-sentiment" style="${formatSentiment(rssNewsItem.sentiment)}">${rssNewsItem.sentiment}</p>
            </div>
            <div class="robinews-top-news-content">
              <div class="robinews-top-news-source-date">
                <span>${rssNewsItem.source}</span> - ${formatDate(rssNewsItem.pubDate)}
              </div>
              <div class="robinews-top-news-content-title">${rssNewsItem.title}</div>
              <div>
                ${rssNewsItem.content && rssNewsItem.content !== 'n/a' ? formatDescription(rssNewsItem.content) : formatDescription(rssNewsItem.contentSnippet)}
              </div>
            </div>
          </div>
        </a>
      `
    )).join('');


    const html = `
      <div class="robinews-top-news">
        <h3>Top News <a target="__blank" href="https://www.robinews.net/"> By Robinews</a></h3>
        <div class="robinews-top-news-container">
          ${premiumNewsHtml}
          ${rssNewsHtml}
        </div>
      </div>
    `;

    jQuery('#robinews-top-news').append(css);
    jQuery('#robinews-top-news').append(html);
  };

  setTimeout(() => {
    loadMarketNews();
  }, 2000);

})( window.Robinews = window.Robinews || {}, jQuery );
