(( Robinews, jQuery ) => {

  const css = `
    <style>
      .wrapper {
        margin: 20px 0;
      }

      .accordion {
        width: auto;
        height: auto;
        margin: 0 auto;
        background: #fff;
        overflow: hidden;
        transition: height 0.3s ease;
        margin-bottom: 5px;
      }

      .accordion .accordion_tab {
        padding: 5px 10px;
        cursor: pointer;
        user-select: none;
        font-size: 11px;
        font-weight: 700;
        text-transform: uppercase;
        letter-spacing: 2px;
        position: relative;
      }

      .accordion .accordion_tab .accordion_arrow {
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        right: 20px;
        width: 15px;
        height: 15px;
        transition: all 0.3s ease;
      }

      .accordion .accordion_tab .accordion_arrow img {
        width: 10px;
        height: 10px;
      }

      .accordion .accordion_tab.active .accordion_arrow {
        transform: translateY(-50%) rotate(180deg);
        right: 24px;
      }

      .accordion.active {
        height: auto;
      }

      .accordion .accordion_content {
        padding: 10px;
        border-top: 1px solid #e9e9e9;
      }

      .accordion .accordion_content .accordion_item {
        margin-bottom: 20px;
      }

      .accordion .accordion_content .accordion_item p.item_title {
        font-weight: 600;
        margin-bottom: 10px;
        font-size: 18px;
        color: #6adda2;
      }

      .accordion .accordion_content .accordion_item p:last-child {
        color: #9a9b9f;
        font-size: 14px;
        line-height: 20px;
      }
    </style>
  `;

  jQuery('body').append(css);

  Robinews.loadAccordions = () => {
    jQuery('.accordion_tab').click(function() {
      jQuery('.accordion_tab').each(function() {
        jQuery(this).parent().removeClass('active');
        jQuery(this).removeClass('active');
      });
      jQuery(this).parent().addClass('active');
      jQuery(this).addClass('active');
    });
  };

})( window.Robinews = window.Robinews || {}, jQuery );


// <div class="accordion ${index === 0 ? 'active' : ''}">
//   <div class="accordion_tab">
//     ${date}
//     <div class="accordion_arrow">
//       <img src="https://i.imgur.com/PJRz0Fc.png" alt="arrow">
//     </div>
//   </div>
//   <div class="accordion_content">
//     <div class="accordion_item">
//       <p class="item_title">Accordion SubTitle</p>
//       <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Architecto quis sed praesentium dolorem hic ipsam maiores magnam voluptatem deleniti sunt.</p>
//     </div>
//     <div class="accordion_item">
//       <p class="item_title">Accordion SubTitle</p>
//       <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Architecto quis sed praesentium dolorem hic ipsam maiores magnam voluptatem deleniti sunt.</p>
//     </div>
//   </div>
// </div>
