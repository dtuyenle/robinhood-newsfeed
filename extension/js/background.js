/* Check whether new version is installed */
chrome.runtime.onInstalled.addListener(function(details) {
  /* other 'reason's include 'update' */
  if (details.reason == "install") {
    /* If first install, set uninstall URL */
    var uninstallGoogleFormLink = 'https://docs.google.com/forms/d/1lf3B_NwJf9GgDnXizn212CC204v-L3zBW4qGDtmdCXc/edit';
    /* If Chrome version supports it... */
    if (chrome.runtime.setUninstallURL) {
      chrome.runtime.setUninstallURL(uninstallGoogleFormLink);
    }
  }
});
