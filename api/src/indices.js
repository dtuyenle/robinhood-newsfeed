const dateFormat = require('dateformat');
const chromium = require('chrome-aws-lambda');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const sleep = require('./util/sleep');

const getHeadless = async (date) => {
  try {
    const browser = await chromium.puppeteer.launch({
      executablePath: process.env.NODE_ENV !== 'dev' ? await chromium.executablePath : undefined,
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      headless: chromium.headless,
    });
    const page = await browser.newPage();
    const result = [];

    let html = '';
    let jsonResponse = null;

    // s&p
    await page.goto(
      `https://query1.finance.yahoo.com/v8/finance/chart/ES=F?region=US&lang=en-US&includePrePost=false&interval=2m&useYfid=true&range=1d&corsDomain=finance.yahoo.com&.tsrc=finance`,
      {
        waitUntil: 'domcontentloaded'
      }
    );
    await page.waitFor(1000);
    html = await page.content();
    jsonResponse = await page.evaluate(() =>  {
      return JSON.parse(document.querySelector('body').innerText);
    });
    result.push({
      id: 'sp500',
      jsonResponse
    });

    // dow jones
    await page.goto(
      `https://query1.finance.yahoo.com/v8/finance/chart/YM=F?region=US&lang=en-US&includePrePost=false&interval=2m&useYfid=true&range=1d&corsDomain=finance.yahoo.com&.tsrc=finance`,
      {
        waitUntil: 'domcontentloaded'
      }
    );
    await page.waitFor(1000);
    html = await page.content();
    jsonResponse = await page.evaluate(() =>  {
      return JSON.parse(document.querySelector('body').innerText);
    });
    result.push({
      id: 'dow',
      jsonResponse
    });


    // nasdaq
    await page.goto(
      `https://query1.finance.yahoo.com/v8/finance/chart/NQ=F?region=US&lang=en-US&includePrePost=false&interval=2m&useYfid=true&range=1d&corsDomain=finance.yahoo.com&.tsrc=finance`,
      {
        waitUntil: 'domcontentloaded'
      }
    );
    await page.waitFor(1000);
    html = await page.content();
    jsonResponse = await page.evaluate(() =>  {
      return JSON.parse(document.querySelector('body').innerText);
    });
    result.push({
      id: 'nasdaq',
      jsonResponse
    });

    await browser.close();

    return result;
  } catch (err) {
    console.error(err);
    await browser.close();
    throw err;
  }
};

module.exports.handler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);
  const key = `indices_stat`;

  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 30, // 30seconds
      connectionTimeout: 100,
    });

    const data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data));
    }

    const result = await getHeadless();

    cacheWrite(cacheClient, key, JSON.stringify(result));
    cacheClient.instance.quit();
    return successResponse(result, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};
