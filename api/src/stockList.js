const fetch = require('node-fetch');
const chromium = require('chrome-aws-lambda');
const MongoClient = require('mongodb').MongoClient;
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');

const nyse = 'https://api.nasdaq.com/api/screener/stocks?tableonly=true&limit=100000&offset=0&exchange=NYSE&download=true';
const amex = 'https://api.nasdaq.com/api/screener/stocks?tableonly=true&limit=100000&offset=0&exchange=AMEX&download=true';
const nasdaq = 'https://api.nasdaq.com/api/screener/stocks?tableonly=true&limit=100000&offset=0&exchange=NASDAQ&download=true';


const getHeadless = async (url) => {
  let browser = null;
  let data = [];

  try {
    browser = await chromium.puppeteer.launch({
      executablePath: process.env.NODE_ENV !== 'dev' ? await chromium.executablePath : undefined,
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      headless: chromium.headless,
    });
    const page = await browser.newPage();
    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

    await page.goto(
      url,
      {
        waitUntil: 'domcontentloaded'
      }
    );
    await page.waitFor(1000);

    data = await page.content();

    const jsonResponse = await page.evaluate(() =>  {
      return JSON.parse(document.querySelector('body').innerText);
    });

    await browser.close();

    return jsonResponse;
  } catch (err) {
    console.error(err, data, url);
    await browser.close();
    throw err;
  }
};

const getStocks = async () => {
  try {
    const nyseData = await getHeadless(nyse);
    nyseData.data.rows = nyseData.data.rows.map(item => ({...item, market: 'NYSE'}))

    const amexData = await getHeadless(amex);
    amexData.data.rows = amexData.data.rows.map(item => ({...item, market: 'AMEX'}))

    const nasdaqData = await getHeadless(nasdaq);
    nasdaqData.data.rows = nasdaqData.data.rows.map(item => ({...item, market: 'NASDAQ'}))

    return nyseData.data.rows.concat(amexData.data.rows).concat(nasdaqData.data.rows);
  } catch(err) {
    console.error(err);
  }
};


module.exports.schedule = async (event) => {

  // Connection URL
  const url = `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@cluster0.aw3g2.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;

  try {
    const data = await getStocks();

    // Client
    const client = new MongoClient(url);

    await client.connect();
    const database = client.db('robinews');
    const collection = database.collection('stocks');
    const ops = [];
    data.forEach((item) => {
      ops.push({
        updateOne: {
          filter: { symbol: item.symbol },
          update: { $set: { ...item, search_field: item.symbol + ' ' + item.name } },
          upsert: true,
        },
      });
    });

    const result = await collection.bulkWrite(ops);
    console.info(result);
    client.close();
  } catch (err) {
    console.error('stock list error', err);
    // client.close();
  }
};




module.exports.stockList = async (event) => {
  const { query } = event.queryStringParameters;
  const querySearch = query.split(' ').map(word => {
    return word.charAt(0).toUpperCase() + word.slice(1);
  }).join(' ');
  const key = `stock_search_${querySearch.split(' ').join('_')}`;

  // Connection URL
  const url = `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@cluster0.aw3g2.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;

  // Client
  const client = new MongoClient(url);

  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 300, // 5mins
      connectionTimeout: 100,
    });

    let data = await cacheRead(cacheClient, key);

    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data));
    }

    await client.connect();
    const database = client.db('robinews');
    const collection = database.collection('stocks');

    const resultName = await collection.find({ name: new RegExp(querySearch, 'i') }).toArray();
    const resultSymbol = await collection.find({ symbol: querySearch.toUpperCase() }).toArray();
    const result = resultSymbol.concat(resultName).slice(0, 50);

    if (result && result.length > 0) {
      cacheWrite(cacheClient, key, JSON.stringify(result));
    }
    cacheClient.instance.quit();
    return successResponse(result);
  } catch(err) {
    return errorResponse(err);
  }
};




module.exports.stockSearch = async (event) => {
  const { query } = event.queryStringParameters;
  const querySearch = query.split(',').map(word => {
    return { symbol: word.toUpperCase() };
  });
  const key = `stock_search_id_${querySearch.join('_')}`;

  // Connection URL
  const url = `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@cluster0.aw3g2.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;

  // Client
  const client = new MongoClient(url);

  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 1, // 5mins 300
      connectionTimeout: 100,
    });

    let data = await cacheRead(cacheClient, key);

    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data));
    }

    await client.connect();
    const database = client.db('robinews');
    const collection = database.collection('stocks');

    const result = await collection.find({
      "$or": querySearch
     }).toArray();

     if (result && result.length > 0) {
      cacheWrite(cacheClient, key, JSON.stringify(result));
    }
    cacheClient.instance.quit();
    return successResponse(result);
  } catch(err) {
    return errorResponse(err);
  }
};

