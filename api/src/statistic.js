const cheerio = require('cheerio');
const chromium = require('chrome-aws-lambda');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const sleep = require('./util/sleep');

const getHeadless = async (stockId) => {
  try {
    const browser = await chromium.puppeteer.launch({
      executablePath: process.env.NODE_ENV !== 'dev' ? await chromium.executablePath : undefined,
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      headless: chromium.headless,
    });
    const page = await browser.newPage();

    await page.goto(
      `https://finance.yahoo.com/quote/${stockId.replace('.', '-')}/key-statistics?p=${stockId.replace('.', '-')}`,
      {
        waitUntil: 'domcontentloaded'
      }
    );
    await page.waitFor(1500);

    const html = await page.content();
    await browser.close();

    return html;
  } catch (err) {
    console.error(err);
    await browser.close();
    throw err;
  }
};

module.exports.handler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);

  const key = `statistic_${stockId.toLowerCase()}`;
  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 86400, // 1 day
      connectionTimeout: 100,
    });

    const data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(data, stockId);
    }

    const html = await getHeadless(stockId.toLowerCase());
    $ = cheerio.load(html);


    const result = $('#Col1-0-KeyStatistics-Proxy').html();

    if (result && result.length > 0) {
      cacheWrite(cacheClient, key, result);
    }
    cacheClient.instance.quit();
    return successResponse(result, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};
