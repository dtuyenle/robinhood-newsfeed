const cheerio = require('cheerio');
const chromium = require('chrome-aws-lambda');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const sleep = require('./util/sleep');

const getHeadless = async (stockId) => {
  try {
    const browser = await chromium.puppeteer.launch({
      executablePath: process.env.NODE_ENV !== 'dev' ? await chromium.executablePath : undefined,
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      headless: chromium.headless,
    });
    const page = await browser.newPage();

    await page.goto(
      `https://www.tipranks.com/stocks/${stockId}/forecast`,
      {
        waitUntil: 'domcontentloaded'
      }
    );
    await page.waitFor(4000);

    const html = await page.content();
    await browser.close();

    return html;
  } catch (err) {
    console.error(err);
    await browser.close();
    throw err;
  }
};

module.exports.handler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);

  const key = `analyst_${stockId.toLowerCase()}`;
  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 86400, // 1 day
      connectionTimeout: 100,
    });

    const data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(data, stockId);
    }

    const html = await getHeadless(stockId.toLowerCase());
    $ = cheerio.load(html);

    const buyingConsensusOriginal = $('.client-components-pie-style__legend').text();
    const buyingConsensusData = buyingConsensusOriginal.replace('Buy', '').replace('Sell', '').replace('Hold', '').trim().split(' ')
    const buyingConsensus = '<p class="robinews-concensus">' + buyingConsensusData[0] + ' Buy ' + buyingConsensusData[1] + ' Hold ' + buyingConsensusData[2] + ' Sell.</p>'
    const analystConsensus = $('.client-components-stock-research-analysts-price-target-style__ptInfoText').html() + ' Total: ' + buyingConsensus;

    if (analystConsensus && analystConsensus.includes('The average price target is')) {
      cacheWrite(cacheClient, key, analystConsensus);
      cacheClient.instance.quit();
      return successResponse(analystConsensus, stockId);
    } else {
      cacheClient.instance.quit();
      return errorResponse('not found');
    }
  } catch(err) {
    return errorResponse(err);
  }
};
