/** @module caching */

const AbstractCacheClient = require('./clients/AbstractCacheClient');
const RedisCacheClient = require('./clients/RedisCacheClient');

module.exports.AbstractCacheClient = AbstractCacheClient;
module.exports.RedisCacheClient = RedisCacheClient;
