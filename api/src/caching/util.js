/**
 * Read data from Redis db.
 * @param {Object} client - Cache client instance.
 */
const cacheRead = async (client, key) => {
  // Check if cached
  const body = await client.get(key);
  if (body) {
    console.info(`Found cached key: ${key}.`);
    return body;
  } else {
    console.info(`Could not find data with key: ${key}.`);
  }
  return null;
};

/**
 * Store data into Redis db.
 * @param {Object} client - Cache client instance.
 */
const cacheWrite = async (client, key, body) => {
  if (body) {
    client.set(key, body);
  }
};

module.exports.cacheRead = cacheRead;
module.exports.cacheWrite = cacheWrite;
