// bvkjrun48v6vtohir280
// c06e54748v6v0bda4550

const { successResponse, errorResponse } = require('../util/httpResponse');
const { RedisCacheClient } = require('../caching');
const { cacheWrite, cacheRead } = require('../caching/util');
const sleep = require('../util/sleep');
const fetch = require('node-fetch');

const getStockData = async () => {
  const data = await fetch('https://finnhub.io/api/v1/stock/symbol?exchange=US&token=bvkjrun48v6vtohir280');
  const json = await data.json();

  return json;
};

module.exports.handler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);

  const key = `all_stock`;
  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 604800, // 1 day
      connectionTimeout: 2000,
    });

    await sleep(1000);

    const data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data), stockId);
    }

    const result = await getStockData();

    cacheWrite(cacheClient, key, JSON.stringify(result));
    cacheClient.instance.quit();
    return successResponse(result, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};
