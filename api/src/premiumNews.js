const cheerio = require('cheerio');
const chromium = require('chrome-aws-lambda');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const sleep = require('./util/sleep');

const getHeadless = async (stockName) => {
  try {
    const browser = await chromium.puppeteer.launch({
      executablePath: process.env.NODE_ENV !== 'dev' ? await chromium.executablePath : undefined,
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      headless: chromium.headless,
    });
    const page = await browser.newPage();

    await page.goto(
      'https://www.google.com/search?rlz=1C5CHFA_enUS927US927&biw=1302&bih=681&tbs=sbd%3A1&tbm=nws&ei=u3PZX9PGF6iG5wKYuJegCg&q=' + stockName + '+stock&oq=' + stockName + '+stock&gs_l=psy-ab.3..0i433i131k1l2j0i433k1j0l3j0i433k1l2j0l2.33667.37914.0.38181.6.6.0.0.0.0.99.469.6.6.0....0...1c.1.64.psy-ab..0.5.368...0i333k1j0i13k1.0.P4_2hdZ_Td8',
      {
        waitUntil: 'domcontentloaded'
      }
    );
    await page.waitFor(2000);

    const html = await page.content();
    await browser.close();

    return html;
  } catch (err) {
    console.error(err);
    await browser.close();
    throw err;
  }
};

module.exports.handler = async (event) => {
  const { stockId, stockName } = event.queryStringParameters;
  console.info(stockName);

  const key = `premium_news_${stockName.split(' ').join('_').toLowerCase()}`;
  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 60, // 2mins
      connectionTimeout: 100,
    });

    const data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data), stockId);
    }

    const html = await getHeadless(stockName);
    console.log(html);
    $ = cheerio.load(html);
    const items = [];

    $('.WlydOe').each(function(i, elem) {
      const image = $(elem).find('img').attr('src');
      const title = $(elem).find('div[role="heading"]').text();
      const description = $(elem).find('.GI74Re').text();
      const date = $(elem).find('.S1FAPd').text();
      const source = $(elem).find('.CEMjEf').text();
      const url = $(elem).attr('href');
      items.push({ url, image, title, description, date, source });
    });

    if (items.length > 0) {
      cacheWrite(cacheClient, key, JSON.stringify(items.reverse()));
    }
    cacheClient.instance.quit();
    return successResponse(items, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};
