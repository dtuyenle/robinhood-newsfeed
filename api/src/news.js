const RssParser = require('rss-parser');
const Sentiment = require('sentiment');
const fetch = require('node-fetch');
const { successResponse, errorResponse } = require('./util/httpResponse');

const parser = new RssParser();
const sentiment = new Sentiment();

const HEADERS = {
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'
};

const NEWS_MAP = {
  bing: 'https://www.bing.com/news/search?format=rss&q=',
  google: 'https://news.google.com/news/feeds?output=rss&q=',
  // nasdaq: 'https://www.nasdaq.com/feed/rssoutbound?symbol=',
};

const getSentiment = (item) => {
  const sentimentAnalyzer = sentiment.analyze(item.contentSnippet);
  let sentimentVal = 'neutral';
  if (sentimentAnalyzer.score === 0) sentimentVal = 'neutral';
  if (sentimentAnalyzer.score < 0) sentimentVal = 'negative';
  if (sentimentAnalyzer.score > 0) sentimentVal = 'positive';
  return {
    sentiment: sentimentVal,
    originalSentiment: sentimentAnalyzer,
  };
};

const getSource = (item, sourceDefault) => {
  return sourceDefault;
};

const getNews = async (stockId) => {
  const sourceUrl = Object.keys(NEWS_MAP).map(sourceId => ({
    url: `${NEWS_MAP[sourceId]}${stockId}`,
    source: sourceId,
  }));
  const data = [];

  for (let count = 0; count < sourceUrl.length; count++) {
    const response = await fetch(sourceUrl[count].url);
    const rssData = await response.text();

    const jsonData = await parser.parseString(rssData);
    jsonData.items.forEach(item => {
      data.push({
        ...item,
        source: getSource(item, sourceUrl[count].source),
      });
    });
  }

  const resultWithSentiment = data.map(item => ({
    ...item,
    ...getSentiment(item),
  }));

  return resultWithSentiment;
};


module.exports.handler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);

  try {
    const data = await getNews(stockId);
    return successResponse(data, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};