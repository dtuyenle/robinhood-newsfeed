const fetch = require('node-fetch');
const dateFormat = require('dateformat');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const { successResponse, errorResponse } = require('./util/httpResponse');
const sleep = require('./util/sleep');

const getWeekUrl = date => `https://www.tipranks.com/api/earnings/getByDate/?name=${date}&country=US`;
const getStockUrl = stockId => `https://www.tipranks.com/api/earnings/getGraphDetailsByTicker/?ticker=${stockId}`;


const getWeekData = async (date) => {
  const response = await fetch(getWeekUrl(date));
  const data = await response.json();

  return data;
};

const getStockData = async (stockId) => {
  const response = await fetch(getStockUrl(stockId));
  const data = await response.json();

  return data;
};

module.exports.earningWeekHandler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);

  try {
    const cacheClient = new RedisCacheClient({
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 864000, // 10 day
      connectionTimeout: 1000,
    });
    await sleep(1000);

    const weekDaysData = [];
    let currDay = new Date();

    for (let i = 0; i < 10; i++) {
      if (i > 0) {
        currDay.setDate(currDay.getDate()+1);
      }
      const formatedDate = dateFormat(currDay, 'yyyy-mm-dd');
      const key = `earnings_week_${formatedDate}`;

      let data = await cacheRead(cacheClient, key);
      if (data) {
        weekDaysData.push(JSON.parse(data));
      } else {
        data = await getWeekData(formatedDate);
        cacheWrite(cacheClient, key, JSON.stringify(data));
        weekDaysData.push(data);
      }
    }

    cacheClient.instance.quit();
    return successResponse(weekDaysData, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};


module.exports.earningStockHandler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);

  const key = `earnings_stock_${stockId.toLowerCase()}`;

  try {
    const cacheClient = new RedisCacheClient({
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 864000, // 10 day
      connectionTimeout: 1000,
    });

    let data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data), stockId);
    }

    data = await getStockData(stockId);
    cacheWrite(cacheClient, key, JSON.stringify(data));
    cacheClient.instance.quit();
    return successResponse(data, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};







// const dateFormat = require('dateformat');
const chromium = require('chrome-aws-lambda');
// const { successResponse, errorResponse } = require('./util/httpResponse');
// const { RedisCacheClient } = require('./caching');
// const { cacheWrite, cacheRead } = require('./caching/util');
// const sleep = require('./util/sleep');

const getHeadless = async (date) => {
  let browser = null;
  let data = [];

  try {
    browser = await chromium.puppeteer.launch({
      executablePath: process.env.NODE_ENV !== 'dev' ? await chromium.executablePath : undefined,
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      headless: chromium.headless,
    });
    const page = await browser.newPage();
    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

    await page.goto(
      `https://api.nasdaq.com/api/calendar/earnings?date=${date}`,
      {
        waitUntil: 'domcontentloaded'
      }
    );
    await page.waitFor(1000);

    data = await page.content();

    const jsonResponse = await page.evaluate(() =>  {
      return JSON.parse(document.querySelector('body').innerText);
    });

    await browser.close();

    return jsonResponse;
  } catch (err) {
    console.error(err, data);
    await browser.close();
    throw err;
  }
};

module.exports.handler = async (event) => {
  const { date } = event.queryStringParameters;

  const key = `earnings_date_${date}`;
  console.info('earnings', key);

  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 604800, // 1 day
      connectionTimeout: 100,
    });

    await sleep(1000);

    const data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data), date, {
        pragma: 'public',
        'Cache-Control': 'public, max-age=300000',
        ETag: date,
        'Content-Type': 'application/json',
      });
    }
    const result = await getHeadless(date);

    cacheWrite(cacheClient, key, JSON.stringify(result.data.rows));
    cacheClient.instance.quit();
    return successResponse(result.data.rows);
  } catch(err) {
    return errorResponse(err);
  }
};
