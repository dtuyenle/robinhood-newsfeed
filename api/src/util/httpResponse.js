const CORS_HEADER = {
  "Access-Control-Allow-Headers" : "Content-Type",
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
};

module.exports.successResponse = (data, stockId, headers = {}) => {
  return {
    statusCode: 200,
    headers: {
      ...CORS_HEADER,
      ...headers,
    },
    body: JSON.stringify({ data, stockId }),
  };
};

module.exports.errorResponse = (err) => {
  console.error('error', err);
  return {
    statusCode: 500,
    headers: CORS_HEADER,
    body: JSON.stringify({ err }),
  };
};

