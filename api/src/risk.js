const fetch = require('node-fetch');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const sleep = require('./util/sleep');

const getUrl = stockId => `https://www.thestreet.com/util/ratings-screener.jsp?wt=json&rows=200&indent=off&q=type:equity+AND+ticker:${stockId}*++AND+LetterGradeSort:[+25+TO+100+]+AND+Growth:[+0+TO+5+]++AND+TotalReturn:[+0+TO+5+]++AND+Efficiency:[+0+TO+5+]++AND+Pricevolatility:[+0+TO+5+]++AND+Solvency:[+0+TO+5+]++AND+Income:[+0+TO+5+]+&fl=ticker,issue_name,LetterGradeRating,CurrentRating,LetterGradeSort,type,Risk,recommendation,exchange&sort=ticker+asc`;
// https://api.thestreet.com/marketdata/2/1?includePartnerContent=true&includeLatestNews=false&start=0&rt=true&max=10&filterContent=false&format=json&s=SSNC&includePartnerNews=false
let data = {};

const getRisk = async (stockId) => {
  if (data[stockId]) return data[stockId];
  const response = await fetch(getUrl(stockId.toUpperCase()));
  data[stockId] = await response.json();

  return data[stockId];
};


module.exports.handler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);

  const key = `risk_${stockId}`;

  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 604800, // 1 day
      connectionTimeout: 100,
    });

    let data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data), stockId);
    }

    data = await getRisk(stockId);

    cacheWrite(cacheClient, key, JSON.stringify(data));
    cacheClient.instance.quit();
    return successResponse(data, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};