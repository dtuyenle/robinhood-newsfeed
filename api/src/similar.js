const cheerio = require('cheerio');
const chromium = require('chrome-aws-lambda');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const MongoClient = require('mongodb').MongoClient;

// Connection URL
const url = `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@cluster0.aw3g2.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`;

// Client
const client = new MongoClient(url);

const getHeadless = async (stockIndicator) => {
  try {
    const browser = await chromium.puppeteer.launch({
      executablePath: process.env.NODE_ENV !== 'dev' ? await chromium.executablePath : undefined,
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      headless: chromium.headless,
    });
    const page = await browser.newPage();

    await page.goto(
      'https://www.google.com/finance/quote/' + stockIndicator,
      {
        waitUntil: 'domcontentloaded'
      }
    );
    await page.waitFor(1000);

    // await page.focus('#search-bar');
    // await page.keyboard.type(stockId);
    // await page.waitFor(250);

    // await page.evaluate(() => document.querySelector('#search-bar').click());
    // await page.waitFor(250);

    // await page.keyboard.press('Enter');
    // await page.waitFor(2000);

    const html = await page.content();
    await browser.close();

    return html;
  } catch (err) {
    console.error(err);
    await browser.close();
    throw err;
  }
};

module.exports.handler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);

  const key = `similar_id_${stockId}`;
  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 345600, // 4 days 345600
      connectionTimeout: 1000,
    });

    const data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data), stockId);
    }


    const client = new MongoClient(url);
    await client.connect();
    const database = client.db('robinews');
    const collection = database.collection('stocks');
    const result = await collection.findOne({
      symbol: stockId.toUpperCase()
    });

    const stockIndicator = stockId + ':' + result.market;

    const html = await getHeadless(stockIndicator);
    $ = cheerio.load(html);
    const items = [];

    $('div[role="listitem"]').each(function(i, elem) {
      const stock = $(elem).find('a').attr('href');
      let stockArr = stock.split('/');
      id = stockArr[stockArr.length - 1].split(':')[0];

      items.push({ stock: id });
    });

    if (items.length > 0) {
      cacheWrite(cacheClient, key, JSON.stringify(items.reverse()));
    }
    cacheClient.instance.quit();

    return successResponse(items, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};
