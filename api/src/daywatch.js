const cheerio = require('cheerio');
const chromium = require('chrome-aws-lambda');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const sleep = require('./util/sleep');

const getHeadless = async (stockId) => {
  try {
    const browser = await chromium.puppeteer.launch({
      executablePath: process.env.NODE_ENV !== 'dev' ? await chromium.executablePath : undefined,
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      headless: chromium.headless,
    });
    const page = await browser.newPage();

    await page.goto(
      `https://tr-frontend-cdn.azureedge.net/home/prod/payload.json?ver=1614458700002`,
      {
        waitUntil: 'domcontentloaded'
      }
    );
    await page.waitFor(1000);

    const jsonResponse = await page.evaluate(() =>  {
      return JSON.parse(document.querySelector('body').innerText);
    });
    await browser.close();

    return jsonResponse;
  } catch (err) {
    console.error(err);
    await browser.close();
    throw err;
  }
};

module.exports.handler = async (event) => {

  const key = `daywatch`;
  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 86400, // 1 day 86400
      connectionTimeout: 100,
    });

    const data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(data);
    }

    const result = await getHeadless();


    if (result && result.length > 0) {
      cacheWrite(cacheClient, key, result);
    }
    cacheClient.instance.quit();
    return successResponse(result);
  } catch(err) {
    return errorResponse(err);
  }
};
