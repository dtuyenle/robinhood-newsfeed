const Reddit = require('reddit');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const sleep = require('./util/sleep');

const getReddit = async (stockName) => {
  const reddit = new Reddit({
    username: process.env.REDDIT_USERNAME,
    password: process.env.REDDIT_PASS,
    appId: process.env.REDDIT_APP_ID,
    appSecret: process.env.REDDIT_APP_SECRET,
    userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36)'
  });

  const res = await reddit.get('/search', {
    before: stockName,
    q: stockName,
    limit: 100,
    count: 10,
    show: 'all',
    sort: 'hot',
    t: 'week',
    include_facets: false,
    sr_detail: false
})

  return res.data.children.map(item => ({
    url: item.data.url,
    html: item.data.selftext_html,
    text: item.data.selftext,
    title: item.data.title,
    created: item.data.created_utc,
  }));
};

module.exports.handler = async (event) => {
  const { stockId, stockName } = event.queryStringParameters;
  console.info(stockId);

  const key = `reddit_${stockId}`;

  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 900, // 15mins
      connectionTimeout: 100,
    });

    let data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data), stockId);
    }

    data = await getReddit(stockName);

    if (data && data.length > 0) {
      cacheWrite(cacheClient, key, JSON.stringify(data));
    }
    cacheClient.instance.quit();
    return successResponse(data, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};
