const cheerio = require('cheerio');
const fetch = require('node-fetch');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const sleep = require('./util/sleep');

const getUrl1 = stockId => `http://www.j3sg.com/Reports/Stock-Insider/Generate.php?DV=yes&tickerLookUp=${stockId.toLowerCase()}&Submit232=GO`;
const getUrl2 = stockId => `http://www.j3sg.com/Reports/Stock-Insider/Generate.php?tickerLookUp=${stockId.toLowerCase()}&pageNumber=2&descending=1&sortBy=PeriodOfReport&checkbox=1&checkbox2=&checkbox3=&checkbox5=1&checkbox6=&checkbox7=&checkbox8=&checkbox9=&checkbox10=&checkbox11=&checkbox12=&checkbox13=&checkbox14=&select=0&select2=0&select4=0&select5=0&textfieldx0=&textfieldx1=&textfieldx2=&dateFrom=12/13/2010&dateTo=12/13/3000&chartDisplay=90&X1=&X2=&X3=&X4=&X5=`;
const getUrl3 = stockId => `http://www.j3sg.com/Reports/Stock-Insider/Generate.php?tickerLookUp=${stockId.toLowerCase()}&pageNumber=3&descending=1&sortBy=PeriodOfReport&checkbox=1&checkbox2=&checkbox3=&checkbox5=1&checkbox6=&checkbox7=&checkbox8=&checkbox9=&checkbox10=&checkbox11=&checkbox12=&checkbox13=&checkbox14=&select=0&select2=0&select4=0&select5=0&textfieldx0=&textfieldx1=&textfieldx2=&dateFrom=12/13/2010&dateTo=12/13/3000&chartDisplay=90&X1=&X2=&X3=&X4=&X5=`;

// Request URL: https://www.gurufocus.com/reader/_api/stocks/US01WD/insider?page=1&per_page=40&sort=date%7Cdesc

const actionMapping = {
  B: 'Buy',
  AB:	'Automatic Buy',
  D: 'Direct Ownership',
  S: 'Sell',
  AS: 'Automatic Sell',
  I: 'Indirect Ownership',
  OE:	'Options Exercised',
  A: 'Acquired',
  IO: 'Initital Ownership',
  D: 'Disposed',
  GD: 'General Dynamics Corporation',
};

const getInsiders = ($) => {
  const insiders = [];
  $('#demo_table').find('tr').each(function(i, elem) {
    if ($(elem).find('td').eq(0).text().trim() !== 'Rank' &&
      $(elem).find('td').eq(0).text().trim() !== '3m +/-' &&
      $(elem).find('td').eq(0).text().trim() !== 'Filer\'s Name' &&
      (
        $(elem).find('td').eq(5).text().trim() === 'B' ||
        $(elem).find('td').eq(5).text().trim() === 'AB' ||
        $(elem).find('td').eq(5).text().trim() === 'S' ||
        $(elem).find('td').eq(5).text().trim() === 'AS'
      )
    ) {
      insiders.push({
        name: $(elem).find('td').eq(0).text().trim(),
        title: $(elem).find('td').eq(1).text().trim(),
        date: $(elem).find('td').eq(3).text().trim(),
        action: actionMapping[$(elem).find('td').eq(5).text().trim()] || '',
        price: $(elem).find('td').eq(6).text().trim(),
        value: $(elem).find('td').eq(7).text().trim(),
        share: $(elem).find('td').eq(9).text().trim(),
        holding: $(elem).find('td').eq(10).text().trim(),
        form: $(elem).find('td').eq(13).find('a').eq(0).attr('href').trim(),
      });
    }
  });
  return insiders;
};

let data = {};

const getInsider = async (stockId) => {
  if (data[stockId]) return data[stockId];
  const response1 = await fetch(getUrl1(stockId));
  const html1 = await response1.text();
  const $1 = cheerio.load(html1);
  const insider1 = getInsiders($1);

  const response2 = await fetch(getUrl2(stockId));
  const html2 = await response2.text();
  const $2 = cheerio.load(html2);
  const insider2 = getInsiders($2);

  const response3 = await fetch(getUrl3(stockId));
  const html3 = await response3.text();
  const $3 = cheerio.load(html3);
  const insider3 = getInsiders($3);

  data[stockId] = [...insider1, ...insider2, ...insider3];

  return data[stockId];
};

module.exports.handler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);

  try {
    const key = `insider_${stockId.toLowerCase()}`;

    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 86400, // 1 day
      connectionTimeout: 100,
    });

    const cachedData = await cacheRead(cacheClient, key);
    if (cachedData) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(cachedData), stockId);
    }

    const data = await getInsider(stockId);

    if (data && data.length > 0) {
      cacheWrite(cacheClient, key, JSON.stringify(data));
    }

    cacheClient.instance.quit();
    return successResponse(data, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};
