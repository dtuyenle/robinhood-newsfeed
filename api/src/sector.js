const fetch = require('node-fetch');
const { successResponse, errorResponse } = require('./util/httpResponse');

const url = 'https://www.alphavantage.co/query?function=SECTOR&apikey=L4PBG6OHWXRK9FTF';
// const url = 'https://financialmodelingprep.com/api/v3/stock/sectors-performance?apikey=5d884dc842d01fb5f4ef1b7d7c0eab02';

let data = null;

const getSector = async () => {
  if (data) return data;
  const response = await fetch(url);
  data = await response.json();

  return data;
};

module.exports.handler = async (event) => {
  try {
    const data = await getSector();
    return successResponse(data);
  } catch(err) {
    return errorResponse(err);
  }
};
