const dateFormat = require('dateformat');
const chromium = require('chrome-aws-lambda');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const sleep = require('./util/sleep');

const getHeadless = async (date) => {
  let browser = null;
  let data = [];

  try {
    browser = await chromium.puppeteer.launch({
      executablePath: process.env.NODE_ENV !== 'dev' ? await chromium.executablePath : undefined,
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      headless: chromium.headless,
    });
    const page = await browser.newPage();
    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

    await page.goto(
      `https://api.nasdaq.com/api/ipo/calendar?date=${date}`,
      {
        waitUntil: 'domcontentloaded'
      }
    );
    await page.waitFor(1000);

    data = await page.content();

    const jsonResponse = await page.evaluate(() =>  {
      return JSON.parse(document.querySelector('body').innerText);
    });

    await browser.close();

    return jsonResponse;
  } catch (err) {
    console.error(err, data, date);
    await browser.close();
    throw err;
  }
};

module.exports.handler = async (event) => {
  let currDay = new Date();
  const formatedMonth = dateFormat(currDay, 'yyyy-mm');
  const key = `ipo_month_${formatedMonth}`;

  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 604800, // 1 day
      connectionTimeout: 100,
    });

    const data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data));
    }

    const result = await getHeadless(formatedMonth);

    const responseData = {};
    result.data.upcoming.upcomingTable.rows.forEach(item => {
      console.log(item);
      const date = dateFormat(item.expectedPriceDate, 'yyyy-mm-dd');
      if (responseData[date]) {
        responseData[date].push(item);
      } else {
        responseData[date] = [item];
      }
    });

    cacheWrite(cacheClient, key, JSON.stringify(responseData));
    cacheClient.instance.quit();
    return successResponse(responseData);
  } catch(err) {
    return errorResponse(err);
  }
};
