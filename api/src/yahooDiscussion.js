const cheerio = require('cheerio');
const chromium = require('chrome-aws-lambda');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const sleep = require('./util/sleep');

const getHeadless = async (stockId) => {
  try {
    const browser = await chromium.puppeteer.launch({
      devtools: false,
      executablePath: process.env.NODE_ENV !== 'dev' ? await chromium.executablePath : undefined,
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      headless: chromium.headless,
    });
    await sleep(1000);
    const page = await browser.newPage();

    await page.waitFor(1000);

    await page.goto(
      `https://finance.yahoo.com/quote/${stockId}/community?p=${stockId}`,
      {
        waitUntil: 'domcontentloaded',
        timeout: 3000000
      }
    );
    await page.waitFor(1000);

    const html = await page.content();
    await browser.close();

    return html;
  } catch (err) {
    console.error(err);
    await browser.close();
    throw err;
  }
};

module.exports.handler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);

  const key = `yahoo_discussion_${stockId.toLowerCase()}`;
  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 6400, // 1 day
      connectionTimeout: 100,
    });

    const data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(data, stockId);
    }

    const html = await getHeadless(stockId.toUpperCase());
    $ = cheerio.load(html);

    const results = [];
    $('.comment').each(function (i, elem) {
      results.push($(this).html());
    });

    if (results) {
      // cacheWrite(cacheClient, key, results.join(''));
      cacheClient.instance.quit();

      return successResponse(results.join(''), stockId);
    } else {
      cacheClient.instance.quit();
      return errorResponse('not found');
    }
  } catch(err) {
    return errorResponse(err);
  }
};
