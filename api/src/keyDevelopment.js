const fetch = require('node-fetch');
const { successResponse, errorResponse } = require('./util/httpResponse');
const { RedisCacheClient } = require('./caching');
const { cacheWrite, cacheRead } = require('./caching/util');
const sleep = require('./util/sleep');

const getUrl = stockId => `https://www.reuters.com/companies/api/getFetchCompanySigDevs/${stockId}.A`;

let data = {};

const getKeyDev = async (stockId) => {
  if (data[stockId]) return data[stockId];
  const response = await fetch(getUrl(stockId.toUpperCase()));
  data[stockId] = await response.json();

  return data[stockId];
};


module.exports.handler = async (event) => {
  const { stockId } = event.queryStringParameters;
  console.info(stockId);

  const key = `key_dev_${stockId}`;

  try {
    const cacheClient = new RedisCacheClient({
      keyFunction: () => key,
      host: process.env.REDIS_URL,
      port: process.env.REDIS_PORT,
      password: process.env.REDIS_PASS,
      ttl: 604800, // 1 day
      connectionTimeout: 100,
    });

    let data = await cacheRead(cacheClient, key);
    if (data) {
      cacheClient.instance.quit();
      return successResponse(JSON.parse(data), stockId);
    }

    data = await getKeyDev(stockId);

    cacheWrite(cacheClient, key, JSON.stringify(data));
    cacheClient.instance.quit();
    return successResponse(data, stockId);
  } catch(err) {
    return errorResponse(err);
  }
};
