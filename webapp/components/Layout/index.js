import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import HeaderNavigation from '../HeaderNavigation';
import Footer from '../Footer';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  content: {
    marginTop: '80px'
  },
  footer: {
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px`,
  },
  link: {
    cursor: 'pointer',
  }
}));

function Layout({ children }) {
  const classes = useStyles();

  return (<div className={classes.root}>
    <Grid container>
      <Grid item xs={12} sm={12}>
        <HeaderNavigation />
      </Grid>
    </Grid>
    <Grid container className={classes.content}>
      {children}
    </Grid>
    <Footer />
  </div>);
}

export default Layout;
