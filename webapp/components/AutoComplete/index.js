import React from 'react';
import Autocomplete from 'react-autocomplete';
import debounce from 'lodash.debounce';
import SearchIcon from '@material-ui/icons/Search';
import CircularProgress from '@material-ui/core/CircularProgress';
import cleanup from '../../util/cleanup';

export default class AutoComplete extends React.Component {

    constructor(props, context) {
      super(props, context);

      this.state = {
        value: "",
        loading: false,
        autocompleteData: []
      };

      this.onChange = this.onChange.bind(this);
      this.onSelect = this.onSelect.bind(this);
      this.getItemValue = this.getItemValue.bind(this);
      this.renderItem = this.renderItem.bind(this);
      this.retrieveDataAsynchronously = debounce(this.retrieveDataAsynchronously.bind(this), 1000);
    }

    async retrieveDataAsynchronously(searchText){
      let url = `https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/stockList?query=${searchText}`;

      try {
        const response = await fetch(url);
        const data = await response.json();
        this.setState({
          loading: false,
          autocompleteData: data.data.slice(0, 10)
        });
      } catch(err) {
        console.error(err);
      }
    }

    onChange(evt, value){
      this.setState({
        value: value,
        loading: true,
        autocompleteData: [],
      }, () => {
        if(value.trim().length > 0) {
          this.retrieveDataAsynchronously(value);
        }
      });
    }

    onSelect(val){
      window.location.href = '/stock/' + val;
    }

    renderItem(item, isHighlighted) {
      return (
        <div style={{
          display: 'flex',
          justifyContent: 'space-between',
          padding: '10px',
          cursor: 'pointer',
          fontSize: '14px',
          background: isHighlighted ? '#efefef' : '#fff'
        }}>
          <div style={{
            marginRight: '10px',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
            <div style={{
              fontSize: '11px',
              marginRight: '5px',
              background: '#666',
              padding: '5px',
              minWidth: '35px',
              textAlign: 'center',
              borderRadius: '5px',
              color: '#fff'
            }}>
              {item.symbol}
            </div>
            <div>{cleanup(item.name)}</div>
          </div>
          <div style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}>
            <div style={{
              display: 'flex',
              marginRight: '15px',
              alignItems: 'center'
            }}>
              {item.lastsale}
            </div>
            <div style={
              item.netchange.includes('-') ?
              {color: '#ef5350', marginRight: '15px'} :
              {color: '#26a69a', marginRight: '15px'}}
            >
              {
                item.netchange.includes('-') ?
                '-$' + item.netchange.replace('-', '') :
                '$' + item.netchange
            }
            </div>
            <div style={
              item.pctchange.includes('-') ?
                {background: '#ef5350', color: '#fff', padding: '5px', borderRadius: '5px'} :
                {background: '#26a69a', color: '#fff', padding: '5px', borderRadius: '5px'}}
            >
              {item.pctchange}
            </div>
          </div>
        </div>
      );
    }

    getItemValue(item){
      return `${item.symbol}`;
    }

    render() {
      return (
        <div style={{position: 'relative'}}>
          <Autocomplete
            getItemValue={this.getItemValue}
            items={this.state.autocompleteData}
            renderItem={this.renderItem}
            value={this.state.value}
            onChange={this.onChange}
            onSelect={this.onSelect}
            inputProps={{
              style: {
                padding:'10px 10px 10px 35px',
                width: '100%',
                fontSize: '16px'
              }
            }}
            wrapperStyle={{
              width: '30%',
              minWidth: '160px'
            }}
            menuStyle={{
              border: '1px solid #ccc',
              borderRadius: '2px',
              background: 'rgba(255, 255, 255, 0.9)',
              padding: '2px 0',
              fontSize: '90%',
              position: 'fixed',
              overflow: 'auto',
              maxHeight: '50%'
            }}
          />
          <div style={{ position: 'absolute', left: '5px', top: '10px' }}>
            {this.state.loading ? <CircularProgress size={20} /> : <SearchIcon />}
          </div>
        </div>
      );
    }
}
