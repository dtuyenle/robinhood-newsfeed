import React from 'react';
import moment from 'moment';
import useSWR from 'swr';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Spinner from '../Spinner';
import fetcher from '../../util/fetcher';


const formatDate = date => {
  return moment.utc(date).format('MM-DD-YYYY');
};

const useStyles = makeStyles((theme) => ({
  newsContainer: {
    padding: theme.spacing(0.5),
    marginTop: '20px',
    overflow: 'auto',
  },
  newsItem: {
    textAlign: 'left',
    cursor: 'pointer',
  },
  newsItemContent: {
    margin: `${theme.spacing(1)}px 0px`,
    padding: `${theme.spacing(1)}px 0px`,
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
  },
}));

const getSentiment = (sentiment) => {
  if (sentiment === 'positive')
    return <span style={{color: '#00bfa5'}}>Bullish</span>
  return <span style={{color: '#c5221f'}}>Bearish</span>
}

export default function Reddit({ stockId, maxHeight }) {
  let id = stockId || 'us';
  let name = stockId || 'us+stock+market';
  const { data, error } = useSWR('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/reddit?stockId=' + id + '&stockName=' + name, fetcher, {
    revalidateOnFocus: false,
  })

  const classes = useStyles();
  if (error) return <div>failed to load</div>
  if (!data) return <div><Spinner /></div>

  return (
    <div className={classes.newsContainer} style={{maxHeight: maxHeight || '1800px'}} >
      {data.data.map(item => {
        return (
          <div key={`${item.title}`} className={classes.newsItem} onClick={() => window.open(item.url)}>
            <Grid container className={classes.newsItemContent}>
              <Grid item xs={12} sm={12}>
                <Typography gutterBottom variant="body2" color="textSecondary">
                  {formatDate(item.created_utc)}
                </Typography>
                <Typography variant="subtitle2">
                  {item.title}
                </Typography>
                <Typography variant="body2" color="textSecondary">
                  <div
                    dangerouslySetInnerHTML={{
                      __html: item.text
                    }}>
                  </div>
                </Typography>
              </Grid>
            </Grid>
          </div>
        );
      })}
    </div>
  );
}
