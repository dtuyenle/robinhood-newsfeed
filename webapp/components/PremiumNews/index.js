import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ListItem from '@material-ui/core/ListItem';
import useSWR from 'swr';
import fetcher from '../../util/fetcher';
import Spinner from '../Spinner';

const useStyles = makeStyles((theme) => ({
  newsContainer: {
    padding: theme.spacing(0.5),
    overflow: 'auto'
  },
  newsItem: {
    textAlign: 'left',
    padding: `${theme.spacing(1)}px ${theme.spacing(0.8)}px`,
    cursor: 'pointer',
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
  },
  newsImageContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  image: {
    borderBottomLeftRadius: '5px',
    borderTopLeftRadius: '5px',
    borderBottomRightRadius: '5px',
    borderTopRightRadius: '5px',
    width: '80%',
  }
}));

export default function PremiumNews({ stockId, maxHeight }) {
  const id = stockId;
  let result = [];
  let err;
  if (id) {
    let stockName = stockId;

    const ids = [`${stockId} news`, `${stockId} stock news`, `${stockName} stock news`];

    ids.forEach((id) => {
      const { data, error } = useSWR('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/premiumNews?stockName=' + id + '&stockId=' + stockId, fetcher, {
        revalidateOnFocus: false,
      })
      result.push(data);
      err = error;
    });
  } else {
    const ids = ['usa stock market', 'USA undervalued stock', 'stock to invest', 'best stock', 'stock market news', 'stock futures', 'stock to buy'];

    ids.forEach((id) => {
      const { data, error } = useSWR('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/premiumNews?stockName=' + id + '&stockId=' + stockId, fetcher, {
        revalidateOnFocus: false,
      })
      result.push(data);
      err = error;
    });
  }

  const classes = useStyles();
  if (err) return <div>failed to load</div>;

  let items = [];
  const checkDups = {};
  result.forEach((data) => {
    if (data) {
      data.data.forEach(item => {
        if (!checkDups[item.title]) {
          items.push(item);
        }
        checkDups[item.title] = true;
      })
    }
  });

  if (items.length === 0) return <div><Spinner /></div>;

  return (
    <div style={{maxHeight: maxHeight || '1800px'}} className={classes.newsContainer}>
      {items.map((item) => {
        return (<ListItem key={item.url} button onClick={() => window.open(item.url)} className={classes.newsItem}>
          <Grid container>
            <Grid item xs={3} sm={3} className={classes.newsImageContainer}>
              <img className={classes.image}  alt="complex" src={item.image} />
            </Grid>
            <Grid item xs={9} sm={9}>
              <Typography gutterBottom variant="body2" color="textSecondary">
                {item.date} - {item.source}
              </Typography>
              <Typography variant="subtitle2">
                {item.title}
              </Typography>
              <Typography variant="body2" color="textSecondary">
                {item.description}
              </Typography>
            </Grid>
          </Grid>
        </ListItem>);
      })}
    </div>
  );
}
