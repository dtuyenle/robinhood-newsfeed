import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Logo from '../Logo';
import SearchWidget from '../../widgets/SearchWidget';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

function HeaderNavigation() {
  const classes = useStyles();

  return (<div className={classes.root}>
    <AppBar style={{ background: '#fff', boxShadow: 'none', borderBottom: '1px solid #e0e0e0' }} position="fixed" color="transparent">
      <Toolbar>
        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
          <Link href="/">
            <Logo />
          </Link>
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          <SearchWidget />
        </Typography>
      </Toolbar>
    </AppBar>
  </div>);
}

export default HeaderNavigation;
