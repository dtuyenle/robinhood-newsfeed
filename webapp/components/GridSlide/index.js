import React from 'react'
import Container from '../Container';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import cleanup from '../../util/cleanup';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  gridListTile: {
    margin: '10px',
    padding: '15px !important',
    border: '1px solid #e0e0e0',
    borderRadius: '5px',

    '&:hover': {
      cursor: 'pointer',
      background: 'rgba(0, 0, 0, 0.04)'
    }
  },
  symbol: {
    background: '#666',
    color: '#fff',
    padding: '4px 9px',
    fontSize: '11px',
    textAlign: 'center',
    borderRadius: '5px',
    width: '29px'
  },
  name: {
    margin: '15px 0',
    fontSize: '15px',
    height: '25px'
  },
  price: {
    margin: '15px 0',
    fontSize: '15px',
    fontWeight: '600',
  },
  changeRed: {
    color: '#fff',
    padding: '5px',
    borderRadius: '5px',
    background: '#ef5350',
    width: '62px',
    textAlign: 'center',
    fontSize: '13px'
  },
  changeBlue: {
    color: '#fff',
    padding: '5px',
    borderRadius: '5px',
    background: '#26a69a',
    width: '62px',
    textAlign: 'center',
    fontSize: '13px'
  },
}));

const GridSlide = ({ data, label, ...props }) => {
  const classes = useStyles();

  return (
    <Container
      {...props}
      label={label}
    >
      <div className={classes.root}>
        <GridList className={classes.gridList} cols={8}>
          {data.map(item => {
            return <GridListTile onClick={() => {
              window.location.href = '/stock/' + item.symbol;
            }} rows={0.9} className={classes.gridListTile} key={item.symbol}>
              <div className={classes.symbol}>{item.symbol}</div>
              <div className={classes.name}>{cleanup(item.name, 3)}</div>
              <div className={classes.price}>{item.lastsale}</div>
              <div className={item.pctchange.includes('-') ? classes.changeRed : classes.changeBlue}>{item.pctchange}</div>
            </GridListTile>
          })}
        </GridList>
      </div>
    </Container>);
};

export default GridSlide;