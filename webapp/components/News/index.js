import React from 'react';
import moment from 'moment';
import useSWR from 'swr';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FixedSizeList } from 'react-window';

import Spinner from '../Spinner';
import fetcher from '../../util/fetcher';

const sortByDate = (array) => {
  array.sort((a, b) => {
    return new Date(b.pubDate) - new Date(a.pubDate);
  });
  return array;
};

const formatDate = date => {
  return moment(date).fromNow().replace(' ago', '').replace('days', 'd').replace('hours', 'h');
};

const useStyles = makeStyles((theme) => ({
  newsContainer: {
    padding: theme.spacing(0.5),
    marginTop: '20px',
  },
  newsItem: {
    textAlign: 'left',
    cursor: 'pointer',
  },
  newsItemContent: {
    margin: `${theme.spacing(1)}px 0px`,
    padding: `${theme.spacing(1)}px 0px`,
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
    '&:hover': {
      background: 'rgba(0, 0, 0, 0.04)',
    }
  },
}));

const getSentiment = (sentiment) => {
  if (sentiment === 'positive')
    return <span style={{color: '#00bfa5'}}>Bullish</span>
  return <span style={{color: '#c5221f'}}>Bearish</span>
}

export default function News({ stockId, height }) {
  let id = stockId || 'us+stock+market';
  const { data, error } = useSWR('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/news?stockId=' + id, fetcher, {
    revalidateOnFocus: false,
  })

  const classes = useStyles();
  if (error) return <div>failed to load</div>
  if (!data) return <div><Spinner /></div>

  const rowData = sortByDate(data.data);

  const renderRow = (props) => {
    const { index, style } = props;
    const item = rowData[index];

    return (
      <div key={`${item.guid}-${index}`} style={style} className={classes.newsItem}>
        <Grid container className={classes.newsItemContent}>
          <Grid item xs={12} sm={12} onClick={() => window.open(item.link)} >
            <Typography gutterBottom variant="body2" color="textSecondary">
              {formatDate(item.pubDate)} - {getSentiment(item.sentiment)}
            </Typography>
            <Typography variant="subtitle2">
              {item.title}
            </Typography>
          </Grid>
        </Grid>
      </div>
    );
  }

  return (
    <div className={classes.newsContainer}>
      <FixedSizeList height={height || 1000} itemSize={90} itemCount={rowData.length}>
        {renderRow}
      </FixedSizeList>
    </div>
  );
}
