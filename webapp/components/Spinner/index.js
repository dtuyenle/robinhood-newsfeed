import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  colorPrimary: {
    color: '#00bfa5 !important'
  }
}));

export default function Spinner() {
  const classes = useStyles();

  return <CircularProgress
    classes={{ colorPrimary: classes.colorPrimary }}
    color="primary"
    disableShrink
  />;
}
