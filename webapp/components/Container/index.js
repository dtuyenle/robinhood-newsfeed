import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(1),
  },
  border: {
    border: `1px solid ${theme.palette.grey[300]}`,
  },
  label: {
    marginBottom: theme.spacing(2),
  },
  sublabel: {
    marginBottom: theme.spacing(3),
  },
  children: {
    border: `1px solid ${theme.palette.grey[300]}`,
    padding: theme.spacing(1),
  },
}));

function Container({ children, border, label, subLabel, ...props }) {
  const classes = useStyles();

  return (<Grid {...props} container className={`${classes.root}`}>
    {label && <Grid item xs={12} sm={12} className={classes.label}>
      <Typography variant="h6" color="textPrimary">
        {label}
      </Typography>
    </Grid>}
    {subLabel && <Grid item xs={12} sm={12} className={classes.sublabel}>
      <Typography variant="body2" color="textSecondary">
        {subLabel}
      </Typography>
    </Grid>}
    <Grid item xs={12} sm={12} className={border ? classes.children : ''}>
      {children}
    </Grid>
  </Grid>);
}

export default Container;
