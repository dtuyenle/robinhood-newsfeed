import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Modal from '../Modal';

const useStyles = makeStyles((theme) => ({
  footer: {
    maxWidth: '500px',
    margin: '0 auto',
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px`,
    justifyContent: 'center',
  },
  link: {
    cursor: 'pointer',
    marginRight: '30px',
    fontSize: '14px',
  },
  content: {
    padding: theme.spacing(2),
  },
}));

function Footer() {
  const classes = useStyles();

  return (<>
    <Divider />

    <Grid container className={classes.footer}>
      <Modal
        triggerElemRenderer={(handleClickOpen) => {
          return <Typography className={classes.link} onClick={handleClickOpen}>About Us</Typography>;
        }}
        contentElemRenderer={() => {
          return <Typography variant="body2" className={classes.content}>
            We are just a small team of developers who are passionate about investing.
            We launched a chrome extension that provides all needed features for Robinhood users.
            Our long goal is to make Robinews a platform that provides everything that an investor needs.
          </Typography>;
        }}
      />
      <Typography className={classes.link} onClick={() => {window.open('/extension/index.html')}}>
        Chrome Extension
      </Typography>
      <Typography className={classes.link} onClick={() => {window.open('https://s3.amazonaws.com/robinews/privacy.html')}}>
        Privacy Policy
      </Typography>
    </Grid>
  </>);
}

export default Footer;
