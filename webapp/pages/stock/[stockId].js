import { useRouter } from 'next/router'
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Layout from '../../components/Layout';
import ConcensusWidget from '../../widgets/ConcensusWidget';
import InsiderWidget from '../../widgets/InsiderWidget';
import FinancialWidget from '../../widgets/FinancialWidget';
import NewsWidget from '../../widgets/NewsWidget';
import ChartWidget from '../../widgets/ChartWidget';
import EPSWidget from '../../widgets/EPSWidget';
import KeyDevelopment from '../../widgets/KeyDevelopment';
import SimilarWidget from '../../widgets/SimilarWidget';
import { TickerWidget } from '../../widgets/TickerWidget';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  mainContainer: {
    padding: theme.spacing(2),
  },
  leftContainer: {
    marginBottom: theme.spacing(2),
  },
  rightContainer: {
    marginBottom: theme.spacing(2),
  },
}));

function Stock() {
  const router = useRouter();
  const { stockId } = router.query;
  const classes = useStyles();

  return (<Layout>
    <Grid container className={classes.mainContainer}>
      <Grid item xs={12} sm={12}>
        <TickerWidget stockId={stockId} />
      </Grid>
    </Grid>
    <Grid container className={classes.mainContainer}>
      <Grid item xs={12} sm={12}>
        <ChartWidget stockId={stockId} />
      </Grid>
    </Grid>

    <Grid container className={classes.mainContainer}>
      <Grid item xs={12} sm={5}>
        <div className={classes.leftContainer}>
          <ConcensusWidget stockId={stockId} />
        </div>
        <div className={classes.leftContainer}>
          <NewsWidget stockId={stockId} maxHeight={2200} newsHeight={2200} />
        </div>
      </Grid>
      <Grid item xs={12} sm={7}>
        <div className={classes.rightContainer}>
          <KeyDevelopment stockId={stockId}/>
        </div>
        <div className={classes.rightContainer}>
          <InsiderWidget stockId={stockId} />
        </div>
        <div className={classes.rightContainer}>
          <FinancialWidget stockId={stockId} />
        </div>
        <div className={classes.rightContainer}>
          <EPSWidget stockId={stockId} />
        </div>
      </Grid>
    </Grid>

    <Grid container className={classes.mainContainer}>
      <Grid item xs={12} sm={12}>
        <SimilarWidget stockId={stockId} />
      </Grid>
    </Grid>
  </Layout>);
}

// this function only runs on the server by Next.js
export const getServerSideProps = async ({params}) => {
  const stockId = params.stockId;
  return {
     props: { stockId }
  }
}


export default Stock;
