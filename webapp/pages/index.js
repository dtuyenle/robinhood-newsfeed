import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Layout from '../components/Layout';
import NewsWidget from '../widgets/NewsWidget';
import MarketMoverWidget from '../widgets/MarketMoverWidget';
import MarketOverviewWidget from '../widgets/MarketOverviewWidget';
import EconomicCalendarWidget from '../widgets/EconomicCalendarWidget';
import { TickerWidget } from '../widgets/TickerWidget';
import IPOWidget from '../widgets/IPOWidget';
import EarningWidget from '../widgets/EarningWidget';
import LivesWidget from '../widgets/LivesWidget';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  topMainContainer: {
    padding: theme.spacing(1),
  },
  mainContainer: {
    padding: theme.spacing(2),
    marginTop: theme.spacing(6),
  },
  leftContainer: {
    marginBottom: theme.spacing(2),
  },
  centerContainer: {
    padding: `0 ${theme.spacing(2)}px`,
  },
  rightContainer: {
    padding: `0 ${theme.spacing(2)}px`,
  }
}));

function HomePage({ post }) {
  const classes = useStyles();

  return (<Layout>
    <Grid container className={classes.topMainContainer}>
      <Grid item xs={12} sm={12}>
        <TickerWidget />
      </Grid>
    </Grid>
    <Grid container className={classes.mainContainer}>
      <Grid item xs={12} sm={3}>
        <div className={classes.leftContainer}>
          <MarketMoverWidget />
        </div>
        <div className={classes.leftContainer}>
          <EconomicCalendarWidget />
        </div>
        <div className={classes.leftContainer}>
          <IPOWidget />
        </div>
      </Grid>
      <Grid item xs={12} sm={6}>
        <div className={classes.centerContainer}>
          <NewsWidget newsHeight={1700} />
        </div>
      </Grid>
      <Grid item xs={12} sm={3}>
        <div className={classes.rightContainer}>
          <MarketOverviewWidget />
        </div>
        <div className={classes.rightContainer}>
          <EarningWidget />
        </div>
      </Grid>
    </Grid>
  </Layout>);
}

export default HomePage;
