import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '../../components/Container';

const useStyles = makeStyles((theme) => ({
  chartContainer: {
    height: '650px',
    width: '100%'
  }
}));

export default function ChartWidget({ stockId, ...props }) {
  const classes = useStyles();

  return (
    <Container
      {...props}
      label=" "
    >

      <div
        id="robinews-chart-info-trading-view"
        dangerouslySetInnerHTML={{
          __html: `
            <!-- TradingView Widget Chart Info BEGIN -->
            <div class="tradingview-widget-container">
              <div style="width: 45px; height: 45px; background: white; position: absolute; right: 5px; top: 5px;"></div>
              <div class="tradingview-widget-container__widget"></div>
              <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/NASDAQ-AAPL/" rel="noopener" target="_blank"><span class="blue-text">AAPL Price Today</span></a> by TradingView</div>
              <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-symbol-info.js" async>
              {
                "symbol": "${stockId}",
                "width": "100%",
                "locale": "en",
                "colorTheme": "light",
                "isTransparent": false
              }
              </script>
            </div>
            <!-- TradingView Widget END -->
          `
        }}>
      </div>

      <div
        className={classes.chartContainer}
        dangerouslySetInnerHTML={{
          __html: `
            <!-- TradingView Widget BEGIN -->
            <div class="tradingview-widget-container">
              <div style="width: 40px; height: 40px; background: white; position: absolute; left: 5px; bottom: 72px; border-radius: 100%; z-index: 999999999;"></div>
              <div id="tradingview_c7c46"></div>
              <div class="tradingview-widget-copyright">
                <a href="https://www.tradingview.com/symbols/NASDAQ-AAPL/" rel="noopener" target="_blank">
                  <span class="blue-text">AAPL Chart</span>
                </a> by TradingView
              </div>
              <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
              <script type="text/javascript">
              new TradingView.widget(
                {
                  "width": "100%",
                  "height": 650,
                  "autosize": false,
                  "symbol": "${stockId}",
                  "interval": "D",
                  "timezone": "Etc/UTC",
                  "theme": "light",
                  "style": "1",
                  "locale": "en",
                  "toolbar_bg": "#f1f3f6",
                  "enable_publishing": false,
                  "withdateranges": true,
                  "allow_symbol_change": true,
                  "container_id": "tradingview_c7c46"
                }
              );
              </script>
            </div>
            <!-- TradingView Widget END -->
          `
        }}>
      </div>
    </Container>
  );
}
