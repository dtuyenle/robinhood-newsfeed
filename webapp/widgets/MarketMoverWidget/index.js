import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '../../components/Container';

const useStyles = makeStyles((theme) => ({
  container: {
    height: '580px',
    width: '100%'
  }
}));

export default function MarketMoverWidget({ stockId, ...props }) {
  const classes = useStyles();

  return (
    <Container
      {...props}
      label="Market Mover"
    >
      <div
        className={classes.container}
        id="robinews-market-mover-trading-view"
        dangerouslySetInnerHTML={{
          __html: `
            <!-- TradingView Widget BEGIN -->
            <div class="tradingview-widget-container">

              <div style="width: 65px; height: 25px; background: transparent; position: absolute; left: 21px; bottom: 294px; border-top-left-radius: 25px; border-bottom-left-radius: 25px;"></div>
              <div style="width: 65px; height: 25px; background: transparent; position: absolute; left: 21px; bottom: 233px; border-top-left-radius: 25px; border-bottom-left-radius: 25px;"></div>
              <div style="width: 65px; height: 25px; background: transparent; position: absolute; left: 21px; bottom: 174px; border-top-left-radius: 25px; border-bottom-left-radius: 25px;"></div>
              <div style="width: 65px; height: 25px; background: transparent; position: absolute; left: 21px; bottom: 174px; border-top-left-radius: 25px; border-bottom-left-radius: 25px;"></div>
              <div style="width: 65px; height: 25px; background: transparent; position: absolute; left: 21px; bottom: 112px; border-top-left-radius: 25px; border-bottom-left-radius: 25px;"></div>
              <div style="width: 65px; height: 25px; background: transparent; position: absolute; left: 21px; bottom: 55px; border-top-left-radius: 25px; border-bottom-left-radius: 25px;"></div>

              <div style="width: 28px; height: 25px; background: white; position: absolute; right: 1px; bottom: 41px; border-top-left-radius: 25px; border-bottom-left-radius: 25px;"></div>
              <div class="tradingview-widget-container__widget"></div>
              <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/stocks-usa/" rel="noopener" target="_blank"><span class="blue-text">Stock Market Today</span></a> by TradingView</div>
              <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-hotlists.js" async>
              {
                "colorTheme": "light",
                "dateRange": "12M",
                "exchange": "US",
                "showChart": true,
                "locale": "en",
                "width": "100%",
                "height": "100%",
                "largeChartUrl": "",
                "isTransparent": false,
                "showSymbolLogo": false,
                "plotLineColorGrowing": "rgba(69, 129, 142, 1)",
                "plotLineColorFalling": "rgba(118, 165, 175, 1)",
                "gridLineColor": "rgba(240, 243, 250, 1)",
                "scaleFontColor": "rgba(120, 123, 134, 1)",
                "belowLineFillColorGrowing": "rgba(162, 196, 201, 0.12)",
                "belowLineFillColorFalling": "rgba(208, 224, 227, 0.12)",
                "symbolActiveColor": "rgba(33, 150, 243, 0.12)"
              }
              </script>
            </div>
            <!-- TradingView Widget END -->
          `
        }}>
      </div>
    </Container>
  );
}



