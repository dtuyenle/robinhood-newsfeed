import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '../../components/Container';

const useStyles = makeStyles((theme) => ({
  container: {
    height: '460px',
    width: '100%'
  }
}));

export default function MarketOverviewWidget({ stockId, ...props }) {
  const classes = useStyles();

  return (
    <Container
      {...props}
      label="Market Overview"
    >
      <div
        className={classes.container}
        id="robinews-market-overview-trading-view"
        dangerouslySetInnerHTML={{
          __html: `
            <!-- TradingView Widget BEGIN -->
            <div class="tradingview-widget-container">
              <div style="width: 45px; height: 32px; background: transparent; position: absolute; left: 40px; bottom: 97px;"></div>
              <div style="width: 45px; height: 32px; background: transparent; position: absolute; left: 47px; bottom: 162px;"></div>
              <div style="width: 45px; height: 32px; background: transparent; position: absolute; left: 40px; bottom: 223px;"></div>
              <div style="width: 45px; height: 32px; background: white; position: absolute; right: 5px; bottom: 37px;"></div>
              <div class="tradingview-widget-container__widget"></div>
              <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/" rel="noopener" target="_blank"><span class="blue-text">Financial Markets</span></a> by TradingView</div>
              <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-market-overview.js" async>
              {
              "colorTheme": "light",
              "dateRange": "12M",
              "showChart": true,
              "locale": "en",
              "largeChartUrl": "",
              "isTransparent": false,
              "showSymbolLogo": true,
              "width": "100%",
              "height": "100%",
              "plotLineColorGrowing": "rgba(69, 129, 142, 1)",
              "plotLineColorFalling": "rgba(118, 165, 175, 1)",
              "gridLineColor": "rgba(240, 243, 250, 1)",
              "scaleFontColor": "rgba(120, 123, 134, 1)",
              "belowLineFillColorGrowing": "rgba(162, 196, 201, 0.12)",
              "belowLineFillColorFalling": "rgba(208, 224, 227, 0.12)",
              "symbolActiveColor": "rgba(33, 150, 243, 0.12)",
              "tabs": [
                {
                  "title": "Indices",
                  "symbols": [
                    {
                      "s": "FOREXCOM:SPXUSD",
                      "d": "S&P 500"
                    },
                    {
                      "s": "FOREXCOM:NSXUSD",
                      "d": "Nasdaq 100"
                    },
                    {
                      "s": "FOREXCOM:DJI",
                      "d": "Dow 30"
                    }
                  ],
                  "originalTitle": "Indices"
                }
              ]
            }
              </script>
            </div>
            <!-- TradingView Widget END -->
          `
        }}>
      </div>
    </Container>
  );
}
