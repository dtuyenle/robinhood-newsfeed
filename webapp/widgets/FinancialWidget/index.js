import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import useSWR from 'swr';
import Grid from '@material-ui/core/Grid';
import Spinner from '../../components/Spinner';
import Container from '../../components/Container';
import fetcher from '../../util/fetcher';

const useStyles = makeStyles((theme) => ({
  concensusContainer: {
    color: '#3c4043',
  },
  note: {
    marginTop: '25px',
    fontSize: '11px',
    color: '#3c4043',
  }
}));

export default function FinancialWidget({ stockId, ...props }) {
  const { data, error } = useSWR('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/statistic?stockId=' + stockId, fetcher, {
    revalidateOnFocus: false,
  })

  const classes = useStyles();
  if (error) return <div>failed to load</div>
  if (!data) return <div><Spinner /></div>

  return (
    <Container
      {...props}
      border
      label="Financial Statistic"
    >
      <div
        className={classes.concensusContainer}
        dangerouslySetInnerHTML={{
          __html: data.data
        }}>
      </div>
      <Grid container justify="space-between" className={classes.note}>
        <Grid item xs={12} sm={3}>
          <div><strong>mrq</strong> = Most Recent Quarter</div>
          <div><strong>ttm</strong> = Trailing Twelve Months</div>
          <div><strong>yoy</strong> = Year Over Year</div>
          <div><strong>lfy</strong> = Last Fiscal Year</div>
          <div><strong>fye</strong> = Fiscal Year Ending</div>
        </Grid>
        <Grid item xs={12} sm={9}>
          <div>1 Data provided by Thomson Reuters.</div>
          <div>2 Data provided by EDGAR Online.</div>
          <div>3 Data derived from multiple sources or calculated by Yahoo Finance.</div>
          <div>4 Data provided by Morningstar, Inc.</div>
          <div>5 Shares outstanding is taken from the most recently filed quarterly or annual report and Market Cap is calculated using shares outstanding.</div>
          <div>6 EBITDA is calculated by Capital IQ using methodology that may differ from that used by a company in its reporting.</div>
        </Grid>
      </Grid>
    </Container>
  );
}
