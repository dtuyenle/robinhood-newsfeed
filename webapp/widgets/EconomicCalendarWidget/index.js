import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '../../components/Container';

const useStyles = makeStyles((theme) => ({
  container: {
    height: '580px',
    width: '100%'
  }
}));

export default function EconomicCalendarWidget({ stockId, ...props }) {
  const classes = useStyles();

  return (
    <Container
      {...props}
      label="Economic Clendar"
    >
      <div
        className={classes.container}
        id="robinews-economic-calendar-view"
        dangerouslySetInnerHTML={{
          __html: `
            <!-- TradingView Widget BEGIN -->
            <div class="tradingview-widget-container">
              <div style="width: 28px; height: 25px; background: white; position: absolute; right: 1px; bottom: 41px; border-top-left-radius: 25px; border-bottom-left-radius: 25px;"></div>
              <div class="tradingview-widget-container__widget"></div>
              <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/markets/currencies/economic-calendar/" rel="noopener" target="_blank"><span class="blue-text">Economic Calendar</span></a> by TradingView</div>
              <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-events.js" async>
              {
                "colorTheme": "light",
                "isTransparent": false,
                "width": "100%",
                "height": "100%",
                "locale": "en",
                "importanceFilter": "-1,0,1",
                "currencyFilter": "USD"
              }
              </script>
            </div>
            <!-- TradingView Widget END -->
          `
        }}>
      </div>
    </Container>
  );
}
