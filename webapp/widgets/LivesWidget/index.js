import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import useSWR from 'swr';
import Spinner from '../../components/Spinner';
import Container from '../../components/Container';
import fetcher from '../../util/fetcher';

import { Line } from 'react-chartjs-2';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});


const getData = (data, labels, colors) => {
  console.log(data);

//   const copy = data.concat([]);
//   copy.sort((a, b) => a - b);
//   data = data.map(item => item - copy[0]);
// console.log('da',data);


  return {
    labels,
    datasets: [{
      data: data,
      backgroundColor: colors,
      borderColor: colors,
      lineTension: 0,
      fill: true,
      borderWidth: 1
    }]
  };
};

const options = {
  responsive: true,
  scales: {
    yAxes: [{
      gridLines: {
        display: false
      },
      ticks: {
        display: false
      },
    }],
    xAxes: [{
      gridLines: {
        display: false
      },
      ticks: {
        display: false
      }
    }]
  },
  legend: {
    display: false
  }
}


export default function LivesWidget({ ...props }) {
  const { data, error } = useSWR('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/daywatch', fetcher, {
    revalidateOnFocus: false,
  });

  const classes = useStyles();
  if (error) return <div>failed to load</div>
  if (!data) return <div><Spinner /></div>


  return (
    <Container
      {...props}
      border
      label="Stocks On The Move"
    >
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="caption table">
          <TableHead>
            <TableRow>
              <TableCell>Company</TableCell>
              <TableCell align="right">Prices</TableCell>
              <TableCell align="right">Rating</TableCell>
              <TableCell align="right">Price Chart</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.data.Home.data.actives.active.map((row) => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                  <div>{row.ticket}</div>
                  <div>{row.name}</div>
                </TableCell>
                <TableCell align="right">
                  <div>{row.price}</div>
                  <div>{row.change.amount} ( {row.change.percent} )</div>
                </TableCell>
                <TableCell align="right">{row.analystRatings.consensus.priceTarget.USD}</TableCell>
                <TableCell align="right">
                  <Line
                    width={200}
                    height={100}
                    data={getData(
                      row.prices.sevenDays.map(item => Math.round(item.price)),
                      row.prices.sevenDays.map(item => Math.round(item.date)),
                      row.prices.sevenDays.map(item => {
                        return String(row.change.percent).includes('-') ? '#ef5350' : '#26a69a';
                      })
                    )}
                    options={options} />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

    </Container>
  );
}
