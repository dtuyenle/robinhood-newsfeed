import React from 'react'
import useSWR from 'swr';
import moment from 'moment';
import { Bar } from 'react-chartjs-2';
import fetcher from '../../util/fetcher';
import Spinner from '../../components/Spinner';
import Container from '../../components/Container';

const options = {
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true,
        stepSize: 60
      },
    }],
    // xAxes: [{
    //   gridLines: {
    //     display: false
    //   }
    // }]
  },
}

const EPSWidget = ({ stockId, ...props }) => {
  const { data, error } = useSWR('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/earning/stock?stockId=' + stockId, fetcher, {
    revalidateOnFocus: false,
  })

  if (error) return <div>failed to load</div>
  if (!data) return <div><Spinner /></div>

  const currYear = moment().format('YYYY');
  const lastYear = moment().subtract(1, 'year').format('YYYY');
  const lastlastYear = moment().subtract(2, 'year').format('YYYY');

  const quarterMapping = [
    `Q4 ${currYear}`, `Q3 ${currYear}`, `Q2 ${currYear}`, `Q1 ${currYear}`,
    `Q4 ${lastYear}`, `Q3 ${lastYear}`, `Q2 ${lastYear}`, `Q1 ${lastYear}`,
    `Q4 ${lastlastYear}`, `Q3 ${lastlastYear}`, `Q2 ${lastlastYear}`, `Q1 ${lastlastYear}`,
  ];

  data.data.reverse().forEach((item, index) => {
    item.label = quarterMapping[index];
  });

  const labels = [];
  const datasets = [];
  const datasets1 = [];
  const datasets2 = [];

  data.data.reverse().filter(item => item.value).forEach(item => {
    labels.push(item.label);
    datasets1.push(item.earningsToolTip.consensusEPSForecast || 0);
    datasets2.push(item.value);
  });

  datasets.push({
    label: 'Expected',
    data: datasets1,
    backgroundColor: '#00584c',
  });
  datasets.push({
    label: 'Reported',
    data: datasets2,
    backgroundColor: '#c55824',
  });

  return (
    <Container
      {...props}
      border
      label="Earning Date History"
    >
      <Bar data={{labels, datasets}} options={options} />
    </Container>);
};

export default EPSWidget;