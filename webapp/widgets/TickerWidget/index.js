import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '../../components/Container';

const useStyles = makeStyles((theme) => ({
  container: {
    height: '100px',
    width: '100%'
  }
}));

const TickerTapeWidget = ({ stockId, ...props }) => {
  const classes = useStyles();

  return (
    <Container
      {...props}
      label=" "
    >
      <div
        id="robinews-ticker-trading-view"
        dangerouslySetInnerHTML={{
          __html: `
          <!-- TradingView Widget BEGIN -->
          <div class="tradingview-widget-container">
            <div style="width: 45px; height: 45px; background: white; position: absolute; right: 5px; top: 5px;"></div>
            <div class="tradingview-widget-container__widget"></div>
            <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com" rel="noopener" target="_blank"><span class="blue-text">Ticker Tape</span></a> by TradingView</div>
            <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-ticker-tape.js" async>
            {
            "symbols": [
              {
                "proName": "FOREXCOM:SPXUSD",
                "title": "S&P 500"
              },
              {
                "proName": "FOREXCOM:NSXUSD",
                "title": "Nasdaq 100"
              },
              {
                "proName": "FOREXCOM:DJI",
                "title": "Dow Jones"
              },
              {
                "proName": "BITSTAMP:BTCUSD",
                "title": "BTC/USD"
              }
            ],
            "showSymbolLogo": true,
            "colorTheme": "light",
            "isTransparent": false,
            "displayMode": "regular",
            "locale": "en"
          }
            </script>
          </div>
          <!-- TradingView Widget END -->
          `
        }}>
      </div>
    </Container>
  );
}

const TickerWidget = ({ stockId, ...props }) => {
  const classes = useStyles();

  return (
    <Container
      {...props}
      label=" "
    >
      <div
        id="robinews-ticker-trading-view"
        dangerouslySetInnerHTML={{
          __html: `
          <!-- TradingView Widget BEGIN -->
          <div class="tradingview-widget-container">
            <div style="width: 45px; height: 32px; background: white; position: absolute; right: 5px; top: 37px;"></div>
            <div class="tradingview-widget-container__widget"></div>
            <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com" rel="noopener" target="_blank"><span class="blue-text">Quotes</span></a> by TradingView</div>
            <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js" async>
            {
            "symbols": [
              {
                "proName": "FOREXCOM:SPXUSD",
                "title": "S&P 500"
              },
              {
                "proName": "FOREXCOM:NSXUSD",
                "title": "Nasdaq 100"
              },
              {
                "proName": "FOREXCOM:DJI",
                "title": "Dow Jones"
              },
              {
                "proName": "FX_IDC:EURUSD",
                "title": "EUR/USD"
              },
              {
                "proName": "BITSTAMP:BTCUSD",
                "title": "BTC/USD"
              }
            ],
            "colorTheme": "light",
            "isTransparent": false,
            "showSymbolLogo": true,
            "locale": "en"
          }
            </script>
          </div>
          <!-- TradingView Widget END -->
          `
        }}>
      </div>
    </Container>
  );
}

export { TickerWidget, TickerTapeWidget };
