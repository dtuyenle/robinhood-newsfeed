import React from 'react'
import useSWR from 'swr';
import Spinner from '../../components/Spinner';
import fetcher from '../../util/fetcher';
import GridSlide from '../../components/GridSlide';
import Container from '../../components/Container';

const SimilarWidget = ({ stockId, ...props }) => {
  const { data: similars } = useSWR('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/similar?stockId=' + stockId, fetcher, {
    revalidateOnFocus: false,
  });

  const { data, error } = useSWR(similars ? 'https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/stockSearch?query=' +
    similars.data.filter(item => !item.stock.includes('.')).map(item => item.stock).join(',')
  : null, fetcher, {
    revalidateOnFocus: false,
  });

  if (error) return <div>failed to load</div>
  if (!data) return <div><Spinner /></div>

  const interests = data.data.slice(0, Math.round(data.data.length / 2));
  const searchs = data.data.slice(Math.floor(data.data.length / 2));

  return (
    <Container
      label="Discover More"
    >
      <GridSlide subLabel="You Might Be Interested In" data={interests} />
      <GridSlide subLabel="People Also Search For" data={searchs} />
    </Container>
  );
};

export default SimilarWidget;