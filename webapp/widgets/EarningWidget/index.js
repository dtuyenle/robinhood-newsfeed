import React, { useState, useEffect } from 'react';
import useSWR from 'swr';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import EventIcon from '@material-ui/icons/Event';
import Spinner from '../../components/Spinner';
import fetcher from '../../util/fetcher';
import Container from '../../components/Container';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxHeight: '800px',
    overflow: 'auto'
  },
}));

const Accordion = withStyles({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: 'rgba(0, 0, 0, .03)',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(0.5),
  },
}))(MuiAccordionDetails);

const getOneWeekDates = () => {
  const result = [];
  const currDate = new Date();
  for (let i = 0; i < 7; i++) {
    if (i > 0) {
      currDate.setDate(currDate.getDate()+1);
    }
    const formatedDate = moment(currDate).format('YYYY-MM-DD');
    result.push(formatedDate);
  }
  return result;
}

export default function EarningWidget({ ... props }) {
  const [expanded, setExpanded] = useState(0);

  const dates = getOneWeekDates();
  const result = {};

  for(let i = 0; i < dates.length; i++) {
    let { data } = useSWR('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/earnings?date=' + dates[i], fetcher, {
      revalidateOnFocus: false,
    });
    if(data && data.data) {
      result[dates[i]] = data.data;
    }
  }

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const classes = useStyles();

  if (result.length === 0) return <div><Spinner /></div>
console.log(result);
  if(!result || (result && Object.keys(result) === 0)) {
    return '';
  }

  return (
    <Container
      {...props}
      label="Earning Calendar"
    >
      {
        Object.keys(result).map((key, index) => {
          const stocks = result[key];
          return (
            <Accordion key={index} expanded={expanded === index} onChange={handleChange(index)}>
              <AccordionSummary>
                <div style={{marginRight: '4px'}}><EventIcon /></div>
                <Typography variant="body2">{moment(key).format('MMMM Do YYYY')}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <List component="nav" className={classes.root}>
                  {
                    stocks.map(stock => {
                      return (<div key={stock.symbol}>
                        <ListItem button dense onClick={() => {
                          window.location.href = '/stock/' + stock.symbol;
                        }}>
                          <ListItemText
                            primary={<div style={{color: '#000', fontWeight: '500'}}>{stock.name}</div>}
                            secondary={
                              <div>
                                <div>
                                  <span >{stock.symbol} - {stock.time.split('-').join(' ').replace('time ', '')}</span>
                                </div>
                                <div>
                                  <span style={{color: '#000'}}>EPS Forecast:</span> {stock.epsForecast || 'NA'} <span style={{color: '#000'}}>-</span> <span style={{color: '#000'}}>EPS LastYear:</span> {stock.lastYearEPS}
                                </div>
                              </div>
                            }
                          />
                        </ListItem>
                        <Divider />
                      </div>);
                    })
                  }
                </List>
              </AccordionDetails>
            </Accordion>
          );
        })
      }
    </Container>
  );
}
