import React from 'react';
import useSWR from 'swr';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/core/styles';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import EventIcon from '@material-ui/icons/Event';
import Spinner from '../../components/Spinner';
import fetcher from '../../util/fetcher';
import Container from '../../components/Container';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
}));

const Accordion = withStyles({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: 'rgba(0, 0, 0, .03)',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(0.5),
  },
}))(MuiAccordionDetails);

export default function IPOWidget({ ... props }) {
  const { data, error } = useSWR('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/ipo', fetcher, {
    revalidateOnFocus: false
  })
  const [expanded, setExpanded] = React.useState(0);

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const classes = useStyles();

  if (error) return <div>failed to load</div>
  if (!data) return <div><Spinner /></div>

  if(!data || !data.data) {
    return '';
  }

  return (
    <Container
      {...props}
      label="IPO Calendar"
    >
      {
        Object.keys(data.data || {}).map((key, index) => {
          const stocks = data.data[key];

          return (
            <Accordion key={key} expanded={expanded === index} onChange={handleChange(index)}>
              <AccordionSummary>
                <div style={{marginRight: '4px'}}><EventIcon /></div>
                <Typography variant="body2">{moment(key).format('MMMM Do YYYY')}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <List component="nav" className={classes.root}>
                  {
                    stocks.map((stock, index) => {
                      return (<div key={index}>
                        <ListItem dense>
                          <ListItemText
                            primary={stock.companyName}
                            secondary={`${stock.proposedTickerSymbol} ${(stock.proposedSharePrice || '').split('-').map(item => `$${item}`).join(' - ')}`}
                          />
                        </ListItem>
                        <Divider />
                      </div>);
                    })
                  }
                </List>
              </AccordionDetails>
            </Accordion>
          );
        })
      }
    </Container>
  );
}
