import React from 'react';
import AutoComplete from '../../components/AutoComplete';

export default function SearchWidget({ ...props }) {
  return (
    <AutoComplete />
  );
}
