import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '../../components/Container';

const useStyles = makeStyles((theme) => ({
  container: {
    height: '200px',
    width: '100%'
  }
}));

export default function ChartWidget({ stockId, ...props }) {
  const classes = useStyles();

  return (
    <Container
      {...props}
      label=" "
    >
      <div
        id="robinews-profile-trading-view"
        className={classes.chartContainer}
        dangerouslySetInnerHTML={{
          __html: `
            <!-- TradingView Widget BEGIN -->
            <div class="tradingview-widget-container">
              <div class="tradingview-widget-container__widget"></div>
              <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/NASDAQ-AAPL/" rel="noopener" target="_blank"><span class="blue-text">AAPL Profile</span></a> by TradingView</div>
              <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-symbol-profile.js" async>
              {
              "symbol": "NASDAQ:AAPL",
              "height": "200px",
              "colorTheme": "light",
              "isTransparent": false,
              "locale": "en"
            }
              </script>
            </div>
            <!-- TradingView Widget END -->
          `
        }}>
      </div>
    </Container>
  );
}
