import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import useSWR from 'swr';
import moment from 'moment';
import Grid from '@material-ui/core/Grid';
import Spinner from '../../components/Spinner';
import Container from '../../components/Container';
import fetcher from '../../util/fetcher';

const useStyles = makeStyles((theme) => ({
  concensusContainer: {
    color: '#3c4043',
  },
  container: {
    marginTop: '25px',
    padding: '0 15px',
    maxHeight: '500px',
    overflow: 'auto'
  },
  item: {
    marginBottom: '16px',
  }
}));

const formatDate = date => {
  return moment(date).fromNow().replace(' ago', '').replace('days', 'd').replace('hours', 'h');
};

export default function KeyDevelopment({ stockId, ...props }) {
  const { data, error } = useSWR('https://m4yzuzs2s1.execute-api.us-east-1.amazonaws.com/prod/keyDevelopment?stockId=' + stockId, fetcher, {
    revalidateOnFocus: false,
  });

  const classes = useStyles();
  if (error) return <div>failed to load</div>
  if (!data) return <div><Spinner /></div>

  const hasData = ((((data || {}).data || {}).market_data || {}).sig_devs || []).length > 0;

  return (
    hasData ? <Container
      {...props}
      border
      label="KeyDevelopment"
    >
      <Grid container justify="space-between" className={classes.container}>
        {((((data || {}).data || {}).market_data || {}).sig_devs || []).map(item => {
          return (<div className={classes.item}>
            <Typography variant="subtitle2">
              {item.headline}
              <Typography gutterBottom variant="body2" color="textSecondary">
                {formatDate(item.last_update)}
              </Typography>
            </Typography>
            <Typography variant="body2" color="textSecondary">
              {item.description}
            </Typography>
          </div>)
        })}
      </Grid>
    </Container> : ''
  );
}
