import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Chip from '@material-ui/core/Chip';
import Container from '../../components/Container';
import News from '../../components/News';
import Reddit from '../../components/Reddit';
import PremiumNews from '../../components/PremiumNews';
// import YahooDiscussion from '../YahooDiscussion';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  colorPrimary: {
    backgroundColor: '#00bfa5 !important'
  },
  marginRight: {
    marginRight: '10px',
  },
}));

function NewsWidget({ stockId, maxHeight, newsHeight, ...props }) {
  const classes = useStyles();
  const [selectedNews, setNews] = useState(0);

  return (<Container
    {...props}
    label="Today's Financial News"
    subLabel={<Grid container spacing={2}>
        <Chip
          label="Market News"
          clickable
          color={selectedNews === 0 ? 'primary' : 'default'}
          onClick={() => setNews(0)}
          className={classes.marginRight}
          classes={{ colorPrimary: classes.colorPrimary }}
        />
        <Chip
          label="Top stories"
          clickable
          color={selectedNews === 1 ? 'primary' : 'default'}
          onClick={() => setNews(1)}
          className={classes.marginRight}
          classes={{ colorPrimary: classes.colorPrimary }}
        />
        <Chip
          label="Reddit"
          clickable
          color={selectedNews === 2 ? 'primary' : 'default'}
          onClick={() => setNews(2)}
          classes={{ colorPrimary: classes.colorPrimary }}
        />
        {/* <Chip
          label="Yahoo Discussion"
          clickable
          color={selectedNews === 1 ? 'primary' : 'default'}
          onClick={() => setNews(2)}
          classes={{ colorPrimary: classes.colorPrimary }}
        /> */}
    </Grid>}
  >
    {selectedNews === 0 && <PremiumNews stockId={stockId} maxHeight={maxHeight} />}
    {selectedNews === 1 && <News height={newsHeight} stockId={stockId} />}
    {selectedNews === 2 && <Reddit maxHeight={maxHeight} stockId={stockId} />}

    {/* {selectedNews === 2 && <YahooDiscussion stockId={stockId} />} */}
  </Container>);
}

export default NewsWidget;
