import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import useSWR from 'swr';
import Spinner from '../../components/Spinner';
import Container from '../../components/Container';
import fetcher from '../../util/fetcher';

const useStyles = makeStyles((theme) => ({
  concensusContainer: {
    padding: theme.spacing(2),
  },
  container: {
    lineHeight: '24px',
    fontSize: '14px',
    color: '#3c4043',
    padding: '10px',
    border: '1px solid #e0e0e0'
  },
}));

export default function YahooDiscussionWidget({ stockId, ...props }) {
  const { data, error } = useSWR('http://localhost:3000/dev/yahooDiscussion?stockId=' + stockId, fetcher, {
    revalidateOnFocus: false,
  })

  const classes = useStyles();
  if (!data) return <div><Spinner /></div>

  return (
    <Container
      {...props}
      label="Yahoo Discussion"
      className={classes.concensusContainer}
    >
      <div
        className={classes.container}
        dangerouslySetInnerHTML={{
          __html: data.data || data.err
        }}>
      </div>
    </Container>
  );
}
