
const cleanup = (name, cutoff = 4) => {
  return name
    .replace('Inc.', '')
    .replace('American depositary shares each  representing one Class A ordinary share', '')
    .replace('Common Stocks', '')
    .replace('Common Stock', '')
    .replace('Common Shares', '')
    .replace('Common Share', '')
    .replace('(The) Common Stock', '')
    .replace('(The)', '')
    .replace('Corporations', '')
    .replace('Corporation', '')
    .replace('Company', '')
    .replace('Holdings', '')
    .replace('Holding', '')
    .replace('Class', '')
    .replace('Group', '')
    .split(' ')
    .slice(0, cutoff)
    .join(' ');
};

export default cleanup;
